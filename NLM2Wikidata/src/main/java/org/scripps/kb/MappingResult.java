/**
 * 
 */
package org.scripps.kb;


/**
 * @author bgood
 *
 */
public class MappingResult {
	public String cui;
	public String qid;
	public String map_type;
	public String link;
	public MappingResult(String cui, String qid, String map_type, String link) {
		this.cui = cui;
		this.qid = qid;
		this.map_type = map_type;
		this.link = link;
		if(map_type.equals("id")
			||map_type.equals("preflabel")
			||map_type.equals("altlabel")
			||map_type.equals("failed")
		   ){	
		}else{
			System.err.println("Bad map type: "+map_type);
		}
		
	} 
	
}
