/**
 * 
 */
package org.scripps.kb;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.scripps.semmed.SemDb;
import org.scripps.semmed.SemRepPredication;
import org.scripps.umls.UmlsDb;

/**
 * @author bgood
 *
 */
public class SemmedExtractor {

	/**
	 * Entry point for manually running the process of pulling wikidata-coded predications out of SemmedDB
	 * @param args
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws IOException, SQLException {
		SemmedExtractor e = new SemmedExtractor();
		//e.buildSemConceptWikidataMapping();
		//e.exportConceptTable();
		//e.extractPreds();
	}
	

	/**
	 * Loads a new database table 'cui_qid' with mappings from the list of cuis
	 * in SemmedDB.  Could be parameterized and abstracted if this needed to be done for other cui sets.
	 * 
	 * This largely a database-oriented version of UmlsWikidataBridge.mapCuisToWikiDataItems
	 * The two could be merged.
	 * Main difference apart from the output going to a database instead of a file is the addition of
	 * aliases from the umls as another step in the label matching process
	 * @param outfile
	 * @throws IOException
	 */
	public void buildSemConceptWikidataMapping() throws IOException{
		boolean uselabelmatching = true; //this is the point, otherwise its fast enough to run everything at once
		UmlsWikidataBridge bridge = new UmlsWikidataBridge();
		//define set of cuis for this run
		SemDb sdb = new SemDb();
		int limit = 10000;
		bridge.cuis = sdb.getUnMappedCUIsInSemmedDB(limit, true,""); //ENTREZ
		System.out.println("cuis to map "+bridge.cuis.size());
		//queries wikidata and builds a map from cui to items
		bridge.buildIdMapping();
		int mapped = 0; int unmapped = 0; int multimapped = 0;	
		int n = 0;
		for(String cui_ : bridge.cuis){
			String mtype = "failed";
			String link = "";
			String cui = cui_;
			//check if its done already and skip if so. 
			//remove to allow updating/synchronization (and implement that..)
			List<MappingResult> alreadydone = sdb.retrieveMapping(cui);
			if(alreadydone!=null){
				continue;
			}			
			boolean isgene = false;
			//handle presence of genes in the mix
			/**
		cuis.add("3604"); //map the gene
		cuis.add("C1335197|5127"); //map the gene if only one
		cuis.add("250|4012|9373"); // toss out multi gene nodes (they haven't figured out what gene it is)
		cuis.add("C0073263|6130|161406|441642"); //use the CUI
		//TODO convert this to allow a multi mapping	
			 */
			if(cui.contains("|")){
				String[] ids = cui.split("\\|");	
				//"C1335197|5127"
				if(ids.length==2){ 
					if(ids[0].startsWith("C")){
						cui = ids[1]; //use the entrez if only one
						isgene = true;
					}
				}else if(ids.length>2){
					if(ids[0].startsWith("C")){
						//C1335197|5127|999
						cui = ids[0]; //use the CUI
					}else{
						//multiple genes, no idea what it is.
						continue;
					}
				}
			}else if(UmlsWikidataBridge.isInteger(cui)){
				isgene = true;
			}
			Map<String, Set<String>> source_ids = new HashMap<String, Set<String>>();
			String name = sdb.getPreferredTerm(cui);				
			bridge.cui_name.put(cui, name);
			Set<String> wikidata_items = new HashSet<String>();
			Set<String> matching_umls_sources = new HashSet<String>();
			//get any direct CUI mappings
			Set<String> direct = new HashSet<String>();
			if(bridge.id_items.get("UMLS_"+cui)!=null){
				direct = bridge.id_items.get("UMLS_"+cui);
			}
			//add any read from cache
			Set<String> cached = bridge.cui_items.get(cui);
			if(cached!=null){
				direct.addAll(cached);
			}
			//WikidataSparqlClient.getItemsByIdProp("P2892", cui);
			if(direct!=null&&direct.size()>0){
				wikidata_items.addAll(direct);
				matching_umls_sources.add("umls");
				Set<String> t = new HashSet<String>();
				t.add(cui);
				source_ids.put("umls", t);
				link = "umls";
				mtype = "id";
			}else{
				//get any indirect mappings
				source_ids = bridge.udb.listIdentifiersForCUI(cui);
				//look up wikidata id properties that match the sources found
				if(source_ids!=null){ 
					//use cached id map
					if(isgene){ //umls likes to just throw entrez ids around like cuis..
						Set<String> items = bridge.id_items.get("entrez_"+cui);
						if(items!=null&&items.size()>0){
							wikidata_items.addAll(items);
							matching_umls_sources.add("entrez");
						}
					}else{
						Set<String> sources_in_wd = new HashSet<String>(source_ids.keySet());
						sources_in_wd.retainAll(bridge.usource_wprop.keySet());
						if(sources_in_wd!=null){
							for(String _source : sources_in_wd){
								String source = _source;
								for(String _id : source_ids.get(source)){
									String id = _id;
									//seems only case like this so far..
									if(source.equals("HGNC")&&_id.startsWith("HGNC")){
										id = id.substring(5);
										source = "HGNC_ID";
									}
									Set<String> items = bridge.id_items.get(source+"_"+id);
									if(items!=null&&items.size()>0){
										wikidata_items.addAll(items);
										matching_umls_sources.add(source);
									}
								}
							}
						}
					}
					if(wikidata_items.size()>0){
						mtype = "id";
						for(String s : matching_umls_sources){
							link+=s+",";
						}
					}
					//no matches on ids, try labels
					if(uselabelmatching){
						if(wikidata_items.size()==0&&name!=null&(!name.equals("NULL"))){
							//try direct match on preferred terms
							Set<String> items = WikidataSparqlClient.getItemsByPreferredOrAltLabel(name);
							link = name;
							mtype = "preflabel";
							if(items==null||items.size()==0){
								String name_lc = name.toLowerCase();
								items = WikidataSparqlClient.getItemsByPreferredOrAltLabel(name_lc);
								mtype = "preflabel";
								link = name_lc;
							}
							if(items!=null&&items.size()>0){
								wikidata_items.addAll(items);
								matching_umls_sources.add(mtype);
							}else{
								//go through aliases from umls
								Set<String> aliases = bridge.udb.getSynset(cui);
								for(String alias : aliases){
									items = WikidataSparqlClient.getItemsByPreferredOrAltLabel(alias);
									if(items!=null&&items.size()>0){
										mtype = "altlabel";
										wikidata_items.addAll(items);
										matching_umls_sources.add(mtype);
										link = alias;
										break;
									}
								}
							}
						}
					}
				}
			}
			bridge.cui_items.put(cui, wikidata_items);
			bridge.cui_sources.put(cui, matching_umls_sources);	

			//global counts
			if(wikidata_items.size()==0){
				unmapped++;
			}else if(wikidata_items.size()==1){
				mapped++;
			}else if(wikidata_items.size()>1){
				multimapped++;
			}
			//save to database
			if(wikidata_items==null||wikidata_items.size()==0){
				MappingResult mr = new MappingResult(cui, "none", "failed", "");
				sdb.insertMappingResult(mr);
			}else{
				for(String item : wikidata_items){
					MappingResult mr = new MappingResult(cui, item, mtype, link);
					sdb.insertMappingResult(mr);
				}
			}
			n++;
			if(n%1000==0){
				System.out.println("Finished\t"+n+"\tcuis");
			}
		}
		//report 
		System.out.println("mapped\tunmapped\tmultimapped");
		System.out.println(mapped+"\t"+unmapped+"\t"+multimapped);
		return;
	}

/**
 * Once the database table cui_qid is populated (e.g. by buildSemConceptWikidataMapping())
 * This method will export the results into tab-delimited file suitable loading concepts into knowledge.bio
 * @throws IOException
 * @throws SQLException
 */
	public void exportConceptTable() throws IOException, SQLException{
		SemDb sdb = new SemDb();
		UmlsDb udb = new UmlsDb();
		int limit = 2000000;
		String map_type = null;  //"id"; "preflabel"; "altlabel";
		Map<String, Set<String>> cui_qid = sdb.getAllCuiQidMappings(map_type, limit);
		//keep all and only one to one mappings based ids or labels
		FileWriter concepts_out = new FileWriter("/Users/bgood/kb2/semmed_import/semmed_all_oneone_concepts.tsv");
		String concept_header = "qid	semantic_groups	preflabel	synonyms	definition";
		concepts_out.write(concept_header+"\n");
		int n = 0;
		System.out.println("cui_qid.size() "+cui_qid.size());
		for(String cui : cui_qid.keySet()){
			n++;
			if(n%25==0){
				System.out.print(n+" ");
			}
			if(n%100==0){
				System.out.println(n);
			}
			Set<String> qids = cui_qid.get(cui);
			//check if entrez
			boolean isgene = false;
			if(UmlsWikidataBridge.isInteger(cui)){
				isgene = true;
			}
			if(qids.size()==1){
				String qid = qids.iterator().next();
				Set<String> groups = udb.getGroupsForCUI(cui, true);
				String preflabel = sdb.getPreferredTerm(cui); //using semmed because its still using 2006 umls..
				Set<String> synset = udb.getSynset(cui);
				String def = "";
				if(!isgene){
					Map<String, String> source_def = sdb.getCUIdefs(cui);
					def = sdb.pickOneDef(source_def);
				}
				String row = qid+"\t"+set2col(groups,"|")+"\t"+preflabel+"\t"+set2col(synset,"|")+"\t"+def+"\n";
				if(isgene){
					row = qid+"\tGENE\t"+preflabel+"\t"+set2col(WikidataSparqlClient.getAliases(qid),"|")+"\thuman gene\n";
				}
				concepts_out.write(row);
			}//else multimapped and needs more processing to sort
		}
		
		concepts_out.close();
	}
	
	public static String set2col(Set<String> things, String delimiter){
		String col = "";
		if(things!=null&&things.size()>0){
			for(String thing : things){
				col+=thing+delimiter;
			}
			col = col.substring(0,col.length()-1);
		}
		return col;
	}

	/**
	prop_map.put("ISA", "P279");
	prop_map.put("CAUSES", "P1542");
	prop_map.put("PART_OF", "P361");
	prop_map.put("USES", "P2283");
	prop_map.put("TREATS", "P2175");
	prop_map.put("DIAGNOSES", "P3356");
	prop_map.put("INTERACTS_WITH", "P129");
	prop_map.put("MANIFESTATION_OF", "P1557");
	prop_map.put("PRODUCES", "P1056");
	prop_map.put("same_as", "P2888");
	prop_map.put("PRECEDES", "P156");
	prop_map.put("LOCATION_OF", "P276");	
	prop_map.put("ASSOCIATED_WITH", "P2293"); //note wikidata prop only applies to genetic elements in the domain
*/

	
/**
 * Once the mappings from CUIs to QID from Wikidata are loaded, this queries SemmedDB by predicate type
 * and exports a table of predications (e.g.  X causes Y reference reference_text).
 * All hard coded, could be abstracted if useful.
 * @throws IOException
 */
public void extractPreds() throws IOException{
	SemDb sdb = new SemDb();		
	List<SemRepPredication> preds = sdb.getByProp("ISA",10000000); 
	System.out.println("Triples loaded = "+preds.size());
	Set<String> cuis = sdb.getCuisFromPreds(preds);
	System.out.println("N cuis in triples = "+cuis.size());
	UmlsWikidataBridge b = new UmlsWikidataBridge();
	b.cuis = cuis;
	//load precomputed mapping
	int limit = 10000000;
	b.loadStoredMapFromDatabase(null, limit);
	FileWriter predicates_out = new FileWriter("/Users/bgood/kb2/semmed_import/semmed_ISA_all.tsv");
	String pred_header = "subject_qid	property_id	object_qid	reference_uri	reference_supporting_text	reference_date";
	predicates_out.write(pred_header+"\n");
	int n = 0;
	for(SemRepPredication pred : preds){
		if(b.cui_items.get(pred.subject_CUI)!=null&&b.cui_items.get(pred.object_CUI)!=null){
			//only allow 1-1 mappings into this one
			if(b.cui_items.get(pred.subject_CUI).size()==1&&b.cui_items.get(pred.object_CUI).size()==1){
				String subject_item = b.cui_items.get(pred.subject_CUI).iterator().next();
				String object_item = b.cui_items.get(pred.object_CUI).iterator().next();
				String property = pred.predicate;
				String link = "https://www.ncbi.nlm.nih.gov/pubmed/"+pred.pmid;
				String property_uri = b.prop_map.get(property);
				String sentence = pred.sentence;
				Calendar cal = pred.pubdate;
				String date = "not set";
				if(cal!=null){
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					date = df.format(cal.getTime());
				}
				String predicate_row = subject_item+"\t"+property_uri+"\t"+object_item+"\t"+link+"\t"+sentence+"\t"+date;
				//System.out.println(row);
				predicates_out.write(predicate_row+"\n");
				n++;
			}
		}
	}
	predicates_out.close();
	System.out.println("saved "+n+" triples");
}
	
}
