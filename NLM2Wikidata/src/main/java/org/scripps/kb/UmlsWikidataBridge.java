/**
 * 
 */
package org.scripps.kb;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.engine.http.QueryEngineHTTP;
import org.scripps.semmed.SemDb;
import org.scripps.semmed.SemRepPredication;
import org.scripps.umls.UmlsDb;

import gov.nih.nlm.uts.webservice.UtsFault_Exception;

/**
 * This class handles the process of mapping between UMLS CUIs and Wikidata items.  
 * @author bgood
 *
 */
public class UmlsWikidataBridge {

	//the main point of this class is to fill up this mapping table linking CUIs to Wikidata ids
	public Map<String, Set<String>> cui_items;
	//map from namespaced identifier keys like umls:C0014544 or mesh:D0008876 to wikidata items
	//used to cache id map from large wikidata queries.
	public Map<String, Set<String>> id_items;
	//tracks where a cui orignated from (e.g. msh, nci, etc.)
	public Map<String, Set<String>> cui_sources;
	//holds the preferred name of the cui
	public Map<String, String> cui_name;
	
	/* Three embedded tables that could be taken out and stored as text files:
		 * (1) usource_wprop is map from the UMLS Source abbreviation (e.g. MSH for MeSH) to 
		 * the corresponding Wikidata property (e.g. P486)
		 * (2) prop_map links predicates from the UMLS semantic network such as CAUSES to 
		 * Wikidata properties such as P1542
		 * (3) usource_sgroups links UMLS source abbreviations to the UMLS Semantic Groups that most of the items
		 * in that source belong to.  e.g. NCBI is short for NCBI taxonomy, hence it maps the group Living Beings
	*/
	public Map<String, String> usource_wprop;
	public Map<String, String> prop_map;
	public Map<String, Set<String>> usource_sgroups;
	
	//these are the cuis we want to map
	public Collection<String> cuis;
	//we use a local copy of the UMLS to access the cui-> source id mappings
	public UmlsDb udb;
	
	/**
	 * Instantiate hard coded mappings and initialize main data structures
	 */
	public UmlsWikidataBridge(){
		udb = new UmlsDb();
		
		cui_items = new HashMap<String, Set<String>>();
		cui_sources = new HashMap<String, Set<String>>();
		usource_sgroups = new HashMap<String, Set<String>>();
		cui_name = new HashMap<String, String>();
		prop_map  = new HashMap<String, String>();

		usource_wprop = new HashMap<String, String>();
		usource_wprop.put("NCBI", "P685");
		usource_wprop.put("MSH", "P486");
		usource_wprop.put("NCI", "P1748");
		usource_wprop.put("ICD10CM", "P494");
		usource_wprop.put("ICD10", "P494");
		usource_wprop.put("ICD10PCS", "P1690");
		usource_wprop.put("ICD9CM", "P1692");
		usource_wprop.put("OMIM", "P492");
		usource_wprop.put("FMA", "P1402");
		usource_wprop.put("GO", "P686");
		usource_wprop.put("MDR", "P3201"); 
		usource_wprop.put("NDFRT", "P2115");
		usource_wprop.put("HGNC", "P353");	// gene symbol	
		usource_wprop.put("HGNC_ID", "P354"); //UMLS MIXES THESE...
		usource_wprop.put("UMLS", "P2892");
		usource_wprop.put("RXNORM", "P3345");
		usource_wprop.put("entrez", "P351");

		prop_map.put("CAUSES", "P1542");
		prop_map.put("ASSOCIATED_WITH", "P2293"); //note wikidata prop only applies to genetic elements in the domain
		prop_map.put("ISA", "P279");
		prop_map.put("PART_OF", "P361");
		prop_map.put("USES", "P2283");
		prop_map.put("TREATS", "P2175");
		prop_map.put("DIAGNOSES", "P3356");
		prop_map.put("INTERACTS_WITH", "P129");
		prop_map.put("MANIFESTATION_OF", "P1557");
		prop_map.put("PRODUCES", "P1056");
		prop_map.put("same_as", "P2888");
		prop_map.put("LOCATION_OF", "P276");
		prop_map.put("PRECEDES", "P156");
		
/*
 * 
 * see definitions on http://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-12-486 appendix

| PREVENTS             | http://purl.obolibrary.org/obo/RO_0003307	solid	'is preventative for condition'
| INHIBITS             | http://purl.obolibrary.org/obo/RO_0002408	solid	'directly inhibits (process to process)'
| STIMULATES           | http://purl.obolibrary.org/obo/RO_0002629 	solid	'directly positively regulates (process to process)'

| AFFECTS              | http://purl.obolibrary.org/obo/RO_0002264	okay	'acts upstream of or within'
| AUGMENTS             | http://purl.obolibrary.org/obo/RO_0002566	okay	'causally influences (material entity to material entity)'

| CONVERTS_TO          |
| COEXISTS_WITH        | http://purl.obolibrary.org/obo/RO_0002610	weak	'correlated with'
| COMPLICATES          | http://purl.obolibrary.org/obo/RO_0002434 	weak	'interacts with'
| DISRUPTS             | http://purl.obolibrary.org/obo/RO_0002410 	weak	'causally related to'
| PREDISPOSES          | cause of (P1542) ? http://purl.obolibrary.org/obo/RO_0002410 	weak	'causally related to' 

| METHOD_OF            | Operative Surgical Procedures METHOD_OF Excision..  LETS LEAVE THESE OUT 
| OCCURS_IN            | http://purl.obolibrary.org/obo/BFO_0000066  ... BUT THESE ARE ALMOST ALL TRIVIAL (occurs in patients) LETS LEAVE THESE OUT
| ADMINISTERED_TO      ||Vaccination  Sheep..  LETS LEAVE THESE OUT 
| PROCESS_OF           | DISEASE PROCESS OF PATIENT..  LETS LEAVE THESE OUT
| NEG_AFFECTS          |
| NEG_PROCESS_OF       |
| NEG_ASSOCIATED_WITH  |
| NEG_STIMULATES       |
| NEG_CAUSES           |
| NEG_TREATS           |
| NEG_PRODUCES         |
| NEG_USES             |
| NEG_PREVENTS         |
| NEG_CONVERTS_TO      |
| NEG_METHOD_OF        |
| NEG_DISRUPTS         |
| NEG_PRECEDES         |
| NEG_ADMINISTERED_TO  |
| NEG_INHIBITS         |
| NEG_COMPLICATES      |
| NEG_PREDISPOSES      |
| NEG_higher_than      |
| NEG_lower_than       |
| NEG_MANIFESTATION_OF |
| NEG_OCCURS_IN        |
| NEG_AUGMENTS         |
| NEG_INTERACTS_WITH   |
| NEG_DIAGNOSES        |
| NEG_COEXISTS_WITH    |
| NEG_PART_OF          |
| NEG_LOCATION_OF      |
| different_from       |
| different_than       |
| higher_from          |
| than_as              |
| higher_than          | LETS LEAVE THESE OUT - not really an assertion...
| lower_than           | LETS LEAVE THESE OUT - not really an assertion...
| compared_with        | LETS LEAVE THESE OUT - not really an assertion...

 		
 */
		//[Disorders]	[Anatomy]	[Physiology]	[Chemicals & Drugs]	[Living Beings]	[Procedures]
		//set expected semantic groups for a given identifier type
		//used for validation
		Set<String> ncbi = new HashSet<String>();
		ncbi.add("Living Beings");
		usource_sgroups.put("NCBI", ncbi);
		usource_sgroups.put("MSH", UmlsDb.groups); //can be any
		usource_sgroups.put("NCI", UmlsDb.groups); //can be any
		Set<String> diseases = new HashSet<String>();
		diseases.add("Disorders");
		usource_sgroups.put("ICD10CM", diseases);
		usource_sgroups.put("ICD10", diseases);
		usource_sgroups.put("ICD10PCS", diseases);
		usource_sgroups.put("ICD9CM", diseases);
		usource_sgroups.put("OMIM", diseases);
		Set<String> anatomy = new HashSet<String>();
		anatomy.add("Anatomy");
		usource_sgroups.put("FMA", anatomy);
		Set<String> phys = new HashSet<String>();
		anatomy.add("Physiology");		
		usource_sgroups.put("GO", phys);
		usource_sgroups.put("MDR", diseases);
		Set<String> chem = new HashSet<String>();
		chem.add("Chemicals & Drugs");	
		usource_sgroups.put("NDFRT", chem);
		usource_sgroups.put("RXNORM", chem);
		Set<String> genes = new HashSet<String>();
		genes.add("Genes & Molecular Sequences");	
		usource_sgroups.put("HGNC", genes);		
		usource_sgroups.put("HGNC_ID", genes);
		usource_sgroups.put("entrez", genes);

		usource_sgroups.put("UMLS", UmlsDb.groups);
		//queries wikidata and builds a mapping table
		//one query for usource
		
	}

	/**
	 * Used for testing.  Could be adapted to make command line application.
	 * @param args
	 * @throws IOException 
	 * @throws UtsFault_Exception 
	 */
	public static void main(String[] args) throws IOException, UtsFault_Exception {	

		UmlsWikidataBridge b = new UmlsWikidataBridge(); 
		System.out.println("Getting cuis");
		b.cuis = b.udb.getAllCuis();
		System.out.println("loaded "+b.cuis.size()+" now mapping");
		
//		int max_score = 5; //(everything including string matching)
//		String cached_map = "/Users/bgood/kb2/semmed_import/umls_wikidata_match_all_prevent_2.tsv";
//		b.loadStoredMap(cached_map, max_score);
		String out_map = "/Users/bgood/kb2/all_umls_ids_only.txt";
		boolean uselabelmatching = false;
		//or build it 
		b.buildIdMapping();
		b.mapCuisToWikiDataItems(out_map, uselabelmatching); 
		//

		//b.mapCuisToWikiDataItems("/Users/bgood/kb2/umls_wikidata_match_sdb_1000.txt"); 
		//umls_wikidata_match_1 was missing NDFRT
	}

	/**
	 * Sets the id_items map with namespaced identifier keys like umls:C0014544 or mesh:D0008876
	 * Mapped to corresponding sets of Wikidata URIs 
	 * This was very helpful optimization as it only executes one sparql query per identifier type
	 * instead of one per actual identifier instance.. 
	 */
	public void buildIdMapping(){
		id_items = new HashMap<String, Set<String>>();
		boolean onlyhuman = false;
		//step through source namespace/property pairs
		for(String source : usource_wprop.keySet()){
			//query for all ids in wikidata and associated items
			//and cache them // e.g. umls:C0014544 -> Q41571
			System.out.println("Mapping "+source);
			if(source.equals("entrez")){ //used to add a constraint to the query engine to only look at human genes
				onlyhuman = true;
			}else{
				onlyhuman = false;
			}
			Map<String, Set<String>> id2_items = WikidataSparqlClient.getIdPropItemMap(source, usource_wprop.get(source), onlyhuman);
			System.out.println(source+"\t"+id2_items.keySet().size());
			id_items.putAll(id2_items);
			id2_items = null;
		}
		System.out.println("Mapped "+id_items.keySet().size()+" wikidata vocabularies");
	}

	/**
	 * Meant for use when iteratively building up a large mapping.  Utilizes custom local table 'cui_qid'
	 * @param map_type
	 * @param limit
	 */
	public void loadStoredMapFromDatabase(String map_type, int limit){
		if(cui_items==null){
			cui_items = new HashMap<String, Set<String>>();
		}
		SemDb sdb = new SemDb();
		cui_items.putAll(sdb.getAllCuiQidMappings(map_type, limit));
	}
	
/**
 * As its name implies. Initializes the cui_items map from a file.  
 * Most likely better to drop this and work with a database.  Legacy here. 
 * @param mapfile
 *e.g.:
cui	preferred name	groups	source:id	n_items	items	n_sources	sources	match
C0445115	Normal saline	[Chemicals & Drugs]	CHV:0000035513,MTH:NOCODE,SCTSPA:262003004,SNOMEDCT_US:70379000,SNOMEDCT_US:387390002,SNOMEDCT_US:262003004,	0		0	10	
C0002716	Amyloid	[Chemicals & Drugs]	MTH:U001687,LNC:LP31745-0,MSH:D000682,MSHSWE:D000682,MSHPOR:D000682,MSHGER:D000682,MSHITA:D000682,MSHFRE:D000682,CHV:0000001068,MSHFIN:D000682,MSHPOL:D000682,MSHCZE:D000682,LCH_NW:sh92004711,MSHRUS:D000682,MSHNOR:D000682,MSHSCR:D000682,NDFRT:N0000169151,MSHSPA:D000682,MSHJPN:D000682,	4	http://www.wikidata.org/entity/Q482050,http://www.wikidata.org/entity/Q2736051,http://www.wikidata.org/entity/Q4749545,http://www.wikidata.org/entity/Q6740853,	1	4	label_match, 
 * 
 * @param max_score
 */
	public void loadStoredMapFromFile(String mapfile, int max_score){
		int n = 0;
		if(cui_items==null){
			cui_items = new HashMap<String, Set<String>>();
		}
		BufferedReader f;
		try {
			f = new BufferedReader(new FileReader(mapfile));
			String line = f.readLine();
			//skip header
			line = f.readLine();
			while(line!=null){
				String[] item = line.split("\t");
				String cui = item[0];
				String prefname = item[1];
				String groups = item[2];
				String source_id_list = item[3];
				String n_wikidata_items = item[4];
				String wikidata_items = item[5];
				String n_mapped_sources = item[6];
				String match_score = item[7];
				String match_type = "";
				if(item.length>8){
					match_type = item[8];
				}

				int score = Integer.parseInt(match_score);
				if(score <= max_score){
					String[] items = wikidata_items.split(",");
					Set<String> itemset = new HashSet<String>();
					for(String q : items){
						if(q.length()>5){ // weeds out cruft from sloppy file writing and excel..
							q = q.replaceAll("\"", "");
							itemset.add(q);
						}
				}
					cui_items.put(cui, itemset);
					n++;
				}
				line = f.readLine();
			}
			f.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("loaded "+n+" cached cuis");
	}

	/**
	 * Sets the cui_items  map from UMLS CUI to set of Wikidata URIs
	 * id_items cache needs to populated first with buildIdMapping to use this 
	 * Writes a file containing the result of the mapping and an ad hoc score for each mapping.  
	 * If uselabelmatching is on, it will try ids first and, if no hits for a CUI, 
	 *   will look for exact matches to the CUI's preferred name among wikidata labels and aliases.
	 * @param outfile
	 * @param uselabelmatching
	 * @throws IOException
	 */
	public void mapCuisToWikiDataItems(String outfile, boolean uselabelmatching) throws IOException{
		//track semantics of mapping errors and successes
		Map<String, Integer[]> group_counts = new HashMap<String, Integer[]>(); //[0] = 1 to 1, [1] = 1 to many, [2] = no match
		Map<String, Integer[]> type_counts = new HashMap<String, Integer[]>(); //[0] = 1 to 1, [1] = 1 to many, [2] = no match
		
		if(id_items==null){
			System.out.println("cache not present");
			return;
		}
		int mapped = 0; int unmapped = 0; int multimapped = 0;	
		FileWriter out = new FileWriter(outfile);//clean it out
		out.write("cui	preferred name	groups	type	source:id	n_items	items	n_sources_matching	score	matching_sources\n");
		int n = 0;
		for(String cui_ : cuis){
			String cui = cui_;
			boolean isgene = false;
			//handle presence of genes in the mix
			/**
		cuis.add("3604"); //map the gene
		cuis.add("C1335197|5127"); //map the gene if only one
		cuis.add("250|4012|9373"); // toss out multi gene nodes (they haven't figured out what gene it is)
		cuis.add("C0073263|6130|161406|441642"); //use the CUI
		//TODO convert this to allow a multi mapping	
			 */
			if(cui.contains("|")){
				String[] ids = cui.split("\\|");	
				//"C1335197|5127"
				if(ids.length==2){ 
					if(ids[0].startsWith("C")){
						cui = ids[1]; //use the entrez if only one
						isgene = true;
					}
				}else if(ids.length>2){
					if(ids[0].startsWith("C")){
						//C1335197|5127|999
						cui = ids[0]; //use the CUI
					}else{
						//multiple genes, no idea what it is.
						continue;
					}
				}
			}else if(isInteger(cui)){
				isgene = true;
			}

			int match_score = 10; //worst
			Map<String, Set<String>> source_ids = new HashMap<String, Set<String>>();
			Set<String> cuigroups = null;
			Set<String> cuitypes = new HashSet<String>();
			if(isgene){
				cuigroups = new HashSet<String>();
				cuigroups.add("Genes & Molecular Sequences");
				cuitypes.add("Gene or Genome");
			}else{
				cuigroups = udb.getGroupsForCUI(cui, false);
				cuitypes = udb.getSemanticTypesFromCUI(cui);
			}
			String name = udb.listPrefNameForCUI(cui);				
			cui_name.put(cui, name);
			Set<String> wikidata_items = new HashSet<String>();
			Set<String> matching_umls_sources = new HashSet<String>();
			//get any direct CUI mappings
			Set<String> direct = new HashSet<String>();
			if(id_items.get("UMLS_"+cui)!=null){
				direct = id_items.get("UMLS_"+cui);
			}
			//add pre-existing mapped cuis from cache
			Set<String> cached = cui_items.get(cui);
			if(cached!=null){
				direct.addAll(cached);
			}
			if(direct!=null&&direct.size()>0){
				match_score = 0; //assume that some one has accomplished the mapping before and we trust it...
				wikidata_items.addAll(direct);
				matching_umls_sources.add("umls");
				Set<String> t = new HashSet<String>();
				t.add(cui);
				source_ids.put("umls", t);
			}else{
				//get any indirect mappings
				source_ids = udb.listIdentifiersForCUI(cui);
				//look up wikidata id properties that match the sources found
				if(source_ids!=null){ 
					//use cached id map
					if(isgene){ //umls likes to just through entrez ids around like cuis..
						Set<String> items = id_items.get("entrez_"+cui);
						if(items!=null&&items.size()>0){
							wikidata_items.addAll(items);
							matching_umls_sources.add("entrez");
						}
					}else{
						Set<String> sources_in_wd = new HashSet<String>(source_ids.keySet());
						sources_in_wd.retainAll(usource_wprop.keySet());
						if(sources_in_wd!=null){
							for(String _source : sources_in_wd){
								String source = _source;
								for(String _id : source_ids.get(source)){
									String id = _id;
									//seems only case like this so far..
									if(source.equals("HGNC")&&_id.startsWith("HGNC")){
										id = id.substring(5);
										source = "HGNC_ID";
									}
									Set<String> items = id_items.get(source+"_"+id);
									if(items!=null&&items.size()>0){
										wikidata_items.addAll(items);
										matching_umls_sources.add(source);
									}
								}
							}
						}
					}

					//If there is a 1-1 identifier match and its in the expected semantic type for the source, 
					//then good chance its right
					if(wikidata_items.size()==1
							&&match_score>0){ //leave the umls mappings alone for score
						//add a semantic type check
						Set<String> intended_groups = new HashSet<String>();
						//though doesn't realy mean anything for things like mesh or nci that have all the types...
						for(String source : matching_umls_sources){
							intended_groups.addAll(usource_sgroups.get(source));
						}
						if(intended_groups.containsAll(cuigroups)){
							match_score = 1; 
						}else{
							match_score = 3; //
						}
						//Possible TODO, add a label matching check

					}
					//no matches on ids, try labels
					if(uselabelmatching){
						if(wikidata_items.size()==0){
							//try direct match on preferred terms
							Set<String> items = WikidataSparqlClient.getItemsByPreferredOrAltLabel(name);
							String mtype = "label_match";
							match_score = 4;
							if(items==null||items.size()==0){
								String name_lc = name.toLowerCase();
								items = WikidataSparqlClient.getItemsByPreferredOrAltLabel(name_lc);
								mtype = "lc_label_match";
								match_score = 5;
							}
							if(items!=null&&items.size()>0){
								wikidata_items.addAll(items);
								matching_umls_sources.add(mtype);
							}else{
								match_score = 10;
							}
						}
					}
				}
			}
			cui_items.put(cui, wikidata_items);
			cui_sources.put(cui, matching_umls_sources);
	//cuigroups		cuitypes
	//group_counts, type_counts [1-1,1-many, none]		
			
			//global counts
			if(wikidata_items.size()==0){
				unmapped++;
			}else if(wikidata_items.size()==1){
				mapped++;
			}else if(wikidata_items.size()>1){
				multimapped++;
			}
			//semantic counts
			for(String group : cuigroups){
				Integer[] group_count = group_counts.get(group);
				if(group_count==null){
					group_count = new Integer[3];
					group_count[0] = 0; group_count[1] = 0; group_count[2] = 0;
				}
				if(wikidata_items.size()==0){
					group_count[2]++;
				}else if(wikidata_items.size()==1){
					group_count[0]++;
				}else if(wikidata_items.size()>1){
					group_count[1]++;
				}		
				group_counts.put(group, group_count);
			}
			
			for(String type : cuitypes){
				Integer[] type_count = type_counts.get(type);
				if(type_count==null){
					type_count = new Integer[3];
					type_count[0] = 0; type_count[1] = 0; type_count[2] = 0;
				}
				if(wikidata_items.size()==0){
					type_count[2]++;
				}else if(wikidata_items.size()==1){
					type_count[0]++;
				}else if(wikidata_items.size()>1){
					type_count[1]++;
				}	
				type_counts.put(type, type_count);
			}

			//System.out.print(cui+"\t"+name+"\t"+cuigroups+"\t"+makeKeyValueString(source_ids)+"\t");
			//System.out.print(cui+"\t"+name+"\t"+cuigroups+"\t\t");
			out.write(cui+"\t"+name+"\t"+cuigroups+"\t"+cuitypes+"\t"+makeKeyValueString(source_ids)+"\t");
			//System.out.print(cui_items.get(cui).size()+"\t");
			out.write(cui_items.get(cui).size()+"\t");
			for(String wd_item : cui_items.get(cui)){
				//System.out.print(wd_item+",");
				out.write(wd_item+",");
			}
			//System.out.print("\t");
			out.write("\t");
			//System.out.print(cui_sources.get(cui).size()+"\t"+match_score+"\t");
			out.write(cui_sources.get(cui).size()+"\t"+match_score+"\t");
			for(String wd_source : cui_sources.get(cui)){
				//System.out.print(wd_source+",");
				out.write(wd_source+",");
			}
			//System.out.print("\n");
			n++;
			if(n%1000==0){
				System.out.println("Finished\t"+n+"\tcuis");
			}
			out.write("\n");
		}
		out.close();
		//report 
		System.out.println("mapped\tunmapped\tmultimapped");
		System.out.println(mapped+"\t"+unmapped+"\t"+multimapped);
		
		//groups
		for(String group : group_counts.keySet()){
			Integer[] c = group_counts.get(group);
			System.out.println(group+"\t"+c[0]+"\t"+c[1]+"\t"+c[2]);
		}
		System.out.println();
		//types
		for(String type : type_counts.keySet()){
			Integer[] c = type_counts.get(type);
			String groups = set2col(udb.getGroupsFromTypeName(type));
			System.out.println(groups+"\t"+type+"\t"+c[0]+"\t"+c[1]+"\t"+c[2]);
		}
		return;
	}


	public String makeKeyValueString(Map<String, Set<String>> key_values){
		String kv_list = "";
		for(String key : key_values.keySet()){
			for(String value : key_values.get(key)){
				kv_list+=key+":"+value+",";
			}
		}
		return kv_list;
	}

	/**
	 * Testing function
	 * @return
	 */
	public static Set<String> setTestCuis(){	

		//
		Set<String> cuis = new HashSet<String>();
		//test case
		//		cuis.add("C0592292"); //no map
		//		cuis.add("C0023434"); //should map
		//		cuis.add("C0000412");
		//		cuis.add("C0000132");
		//		cuis.add("C0000098");
		//		cuis.add("C0000376");
		//		cuis.add("C0000379");
		//cuis.add("C0000184");
		//cuis.add("C0000970"); 
		//Acetaminophen MSH           | D000082 NCI           | C198 NDFRT         | N0000145898 N0000007359 
		//N0000145898
		//N0000145898
		//cuis.add("C0000402"); //map the cui
		//cuis.add("3604"); //map the gene
		//cuis.add("C1335197|5127"); //map the gene if only one
		//cuis.add("250|4012|9373"); // toss out multi gene nodes (they haven't figure out what gene it is)
		//cuis.add("C0073263|6130|161406|441642"); //unless there is a cui - try to map the cui 
		cuis.add("C1366532"); //FASTK Gene
		return cuis;
	}

	public static boolean isInteger(String s) {
		try { 
			Integer.parseInt(s); 
		} catch(NumberFormatException e) { 
			return false; 
		} catch(NullPointerException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}
	
	public static String set2col(Set<String> things){
		String col = "";
		if(things!=null&&things.size()>0){
			for(String thing : things){
				col+=thing+",";
			}
			col = col.substring(0,col.length()-1);
		}
		return col;
	}
}
