/**
 * 
 */
package org.scripps.semmed;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author bgood
 *
 */
public class EuadrProcessor {
	/**
	 * Column  1: Associarion type (PA, NA, SA or FA).
Positive association (PA): the sentence clearly states that there is an association between the entities.
Negative association (NA): the sentence clearly states that there is no association between the entities.
Speculative association (SA): the sentence describes a putative relationship between the target and the disease. 
This might be confirmed or refuted later in the abstract, but in the sentence under study the relationship is presented as a speculation.

Column  2: Number of PubMedID .
Column  3: Number of sentence in the abstract.
Column  4: Text of entity1 in the sentence.
Column  5: Begin offset of entity1 at 'sentence level'
Column  6: End offset of entity1 at 'sentence level'
Column  7: Entity1 type.
Column  8: Text of entity1 in the sentence.
Column  9: Begin offset of entity1 at 'sentence level'.
Column 10: End offset of entity1 at 'sentence level'.
Column 11: Entity1 type.
Column 12: Sentence.
	 */
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String file = "data/gold_gdd_annos/EUADR_Corpus_IBIgroup/EUADR_target_disease.csv";
		List<PubmedDoc> edocs = getEuadrDocs(file);
		for(PubmedDoc doc : edocs){
			System.out.println(doc.PMID+" "+doc.predications.size());  
			for(SemRepPredication p : doc.predications){
				System.out.println("\t"+p.subject_text+"\t"+p.object_text);
			}
		}
	}

	public static List<PubmedDoc> getEuadrDocs(String file){
		List<PubmedDoc> docs = new ArrayList<PubmedDoc>();

		BufferedReader f;
		try {
			f = new BufferedReader(new FileReader(file));
			String line = f.readLine();
			Map<Integer, List<SemRepPredication>> doc_preds = new HashMap<Integer, List<SemRepPredication>>();
			while(line!=null){
				String[] item = line.split("\t");
				String kind = stripQuotes(item[0]);
				String subject_type = stripQuotes(item[6]);
				if((!kind.equals(" FA"))//guessing false association..
						&&(subject_type.equals("Genes & Molecular Sequences")) //ignoring snps "SNP & Sequence variations" for now.
						){
					int pmid = Integer.parseInt(item[1]);
					String concept1 = stripQuotes(item[3]);
					String concept2 = stripQuotes(item[7]);
					List<SemRepPredication> predications = doc_preds.get(pmid);
					if(predications==null){
						predications = new ArrayList<SemRepPredication>();
					}
					SemRepPredication pred = new SemRepPredication();
					pred.object_text = concept2;
					pred.subject_text = concept1;
					pred.predicate = "ASSOCIATED_WITH";  //assuming a gene disease association
					if(kind.equals("NA")){
						pred.predicate = "NEG_ASSOCIATED_WITH";
					}
					predications.add(pred);
					doc_preds.put(pmid, predications);
				}else{
//					System.out.println(kind);
//					System.out.println(subject_type);
				}
				line = f.readLine();
			}
			for(Integer p : doc_preds.keySet()){
				PubmedDoc pd = new PubmedDoc();
				pd.PMID = p;
				pd.predications = doc_preds.get(p);
				docs.add(pd);
			}
			f.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return docs;
	}
	
	public static String stripQuotes(String q){
		String o = q;
		if(q.startsWith("\"")&&q.endsWith("\"")){
			o = q.substring(1,q.length()-1);
		}
		return o;
	}

}
