/**
 * 
 */
package org.scripps.semmed;

import java.util.List;
import java.util.Set;

/**
 * @author bgood
 *
 */
public class PubmedDoc {

	int PMID;
	//small
	Set<String> triples;
	//large
	List<SemRepPredication> predications;

}
