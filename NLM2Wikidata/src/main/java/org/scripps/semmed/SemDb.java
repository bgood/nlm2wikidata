/**
 * 
 */
package org.scripps.semmed;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.scripps.kb.MappingResult;
import org.scripps.kb.UmlsWikidataBridge;
import org.scripps.umls.SemanticType;

import com.mysql.jdbc.Statement;

/**
 * 
 * Collection of convenience methods for interacting with a local version of the Semantic Medline database.
 * 2 main use cases: extracting triples for verification by crowdsourcing and mapping CUIs to wikidata items
 * 
 * Database can be retrieved from https://skr3.nlm.nih.gov/SemMedDB/dbinfo.html 
 * Two additional tables added during the process of mapping large sets of CUIs used in SemmedDB 
 * to corresponding items in wikidata: 
 * mysql> desc used_cuis;
+-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| cui   | varchar(255) | NO   | PRI | NULL    |       |
+-------+--------------+------+-----+---------+-------+
1 row in set (0.00 sec)

mysql> desc cui_qid;
+----------+-------------+------+-----+---------+----------------+
| Field    | Type        | Null | Key | Default | Extra          |
+----------+-------------+------+-----+---------+----------------+
| cui      | varchar(20) | YES  | MUL | NULL    |                |
| qid      | varchar(20) | YES  |     | NULL    |                |
| map_type | varchar(20) | YES  |     | NULL    |                |
| link     | varchar(50) | YES  |     | NULL    |                |
| id       | int(11)     | NO   | PRI | NULL    | auto_increment |
+----------+-------------+------+-----+---------+----------------+
5 rows in set (0.01 sec)

 * @author bgood
 *
 */
public class SemDb {

	java.sql.Connection con;
	String dburl = "jdbc:mysql://localhost:3306/semmedVER26";
	PreparedStatement get_cui_defs ;
	//create table pdist (id int(10) NOT NULL AUTO_INCREMENT primary key, term1 varchar(500), term2 varchar(500), intersect_query varchar(1000), term1_count int, term2_count int, intersect_count int, distance double, time timestamp not null, constraint f1 unique (term1, term2));
	PreparedStatement insertMappingResult;
	PreparedStatement getMappingResult;
	PreparedStatement getPreferredTerm;
	
	public SemDb() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch(java.lang.ClassNotFoundException e) {
			System.err.print("ClassNotFoundException: ");
			System.err.println(e.getMessage());
		}
		try {
			//con = DriverManager.getConnection(dburl,"same", "same");
			con = DriverManager.getConnection(dburl,"root", "");
			String def_query = "select * from umls2016.MRDEF where CUI = ?";
			get_cui_defs = con.prepareStatement(def_query);
			//cui varchar(20), qid varchar(20),map_type varchar(20), link varchar(50)
			String insert_mapping = "insert into cui_qid values(?, ?, ?, ?, ?) ";
			insertMappingResult = con.prepareStatement(insert_mapping);
			String get_mapping = "select * from cui_qid where cui = ?";
			getMappingResult = con.prepareStatement(get_mapping);
			
		String getpreferred = "select PREFERRED_NAME from CONCEPT where CUI = ?";
		getPreferredTerm = con.prepareStatement(getpreferred);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SemDb d = new SemDb();
		//test..
		System.out.println(d.getUnMappedCUIsInSemmedDB(5, false, "asc"));
		
		//String cui = "c"; String qid = "q"; String map_type = "m"; String link = "l";
		//MappingResult m = new MappingResult(cui, qid, map_type, link);
		//d.insertMappingResult(m);
		//Set<SemRepPredication> preds = d.getByProp("ASSOCIATED_WITH",10); 
		//export sample for crowdflower verification
		//String outfile = "data/semmed_verify/test_pred_704116.tsv";
		//d.exportForVerification(outfile);
		//		List<Integer> pmids = new ArrayList<Integer>();
		//		pmids.add(14819754); pmids.add(12255545); pmids.add(13040397);
		//		List<PubmedDoc> pdocs = d.getTriplesForPMIDList(pmids, false);
	}

	/**
	 * create mapping table
	 * create table cui_qid (cui varchar(20), qid varchar(20),map_type varchar(20), link varchar(50));
	 */
	
	public void insertMappingResult(MappingResult m){
		try {
			insertMappingResult.clearParameters();
			insertMappingResult.setString(1, m.cui);
			insertMappingResult.setString(2, m.qid);
			insertMappingResult.setString(3, m.map_type);
			if(m.link.length()>50){
				m.link = "...";
//				String tmp = "";
//				String[] links = m.link.split(",");
//				if(links.length>1){
//					tmp = links[0]+"...";
//				}
//				m.link = tmp;
			}
			insertMappingResult.setString(4, m.link);
			insertMappingResult.setString(5, null); //auto key
			insertMappingResult.execute();
		} catch(SQLException ex) {
			System.err.println("insertMappingResult -----SQLException----- ");
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		
	}
	
	public Map<String, Set<String>> getAllCuiQidMappings(String map_type, int limit){
		Map<String, Set<String>> cui_qids = new HashMap<String, Set<String>>();	
		try {
			String q = "select * from cui_qid where map_type != 'failed' limit "+limit;
			if(map_type!=null){
				q ="select * from cui_qid where map_type = '"+map_type+"' limit "+limit;
			}
			//  | cui      | qid       | map_type | link | id  |
			PreparedStatement getmappings = con.prepareStatement(q);
			ResultSet r = getmappings.executeQuery();			
			while(r.next()){
				String cui = r.getString("cui");
				String qid = r.getString("qid");
				//String map_type = r.getString("map_type");
				//String link = r.getString("link");
				Set<String> qids = cui_qids.get(cui);
				if(qids==null){
					qids = new HashSet<String>();
				}
				qids.add(qid);
				cui_qids.put(cui, qids);
			}
		} catch(SQLException ex) {
			System.err.println("getMappings semmed -----SQLException----- ");
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return cui_qids;
	}
	
	public List<MappingResult> retrieveMapping(String cui){
		List<MappingResult> mappings = null;
		
		try {
			getMappingResult.clearParameters();
			getMappingResult.setString(1, cui);
			ResultSet r = getMappingResult.executeQuery();
			while(r.next()){
				if(mappings==null){
					mappings = new ArrayList<MappingResult>();
				}
				String qid = r.getString("qid");
				String map_type = r.getString("map_type");
				String link = r.getString("link");
				MappingResult m = new MappingResult(cui, qid, map_type, link);
				mappings.add(m);
			}
		} catch(SQLException ex) {
			System.err.println("getMappingResult -----SQLException----- ");
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return mappings;
	}
	
	public String getPreferredTerm(String cui){
		String pref = null;
		
		try {
			getPreferredTerm.clearParameters();
			getPreferredTerm.setString(1, cui);
			ResultSet r = getPreferredTerm.executeQuery();
			if(r.next()){
				pref = r.getString("PREFERRED_NAME");
			}
		} catch(SQLException ex) {
			System.err.println("getPreferredTerm semmed -----SQLException----- ");
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return pref;
	}
	
	public Set<String> getCuisFromPreds(Collection<SemRepPredication> preds){
		Set<String> cuis = new HashSet<String>();
		for(SemRepPredication pred : preds){
			cuis.add(pred.subject_CUI);
			cuis.add(pred.object_CUI);
		}
		return cuis;
	}
	
	public List<PubmedDoc> getTriplesForPMIDList(List<Integer> pmids, boolean cuis){
		List<PubmedDoc> pdocs = new ArrayList<PubmedDoc>(pmids.size());
		if(cuis){
			String q = "SELECT * FROM PREDICATION_AGGREGATE WHERE PMID = ?";
			try {
				PreparedStatement get_by_pmid = con.prepareStatement(q);
				for(Integer pmid : pmids){
					get_by_pmid.clearParameters();
					get_by_pmid.setString(1,pmid+"");
					ResultSet rslt = get_by_pmid.executeQuery();
					PubmedDoc pdoc = new PubmedDoc();
					pdoc.PMID = pmid;
					pdoc.triples = new HashSet<String>();
					while(rslt.next()){
						pdoc.triples.add(rslt.getString("s_cui")+"_"+rslt.getString("predicate")+"_"+rslt.getString("o_cui"));
					}
					pdocs.add(pdoc);
				}
				get_by_pmid.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			String q = "select SENTENCE_PREDICATION.*, SENTENCE.PMID, PREDICATION.PREDICATE "
					+ "from PREDICATION "
					+ "INNER join SENTENCE_PREDICATION on PREDICATION.PREDICATION_ID = SENTENCE_PREDICATION.PREDICATION_ID "
					+ "INNER JOIN SENTENCE ON SENTENCE.SENTENCE_ID = SENTENCE_PREDICATION.SENTENCE_ID "
					+  " where SENTENCE.PMID = ? ";
			try {
				PreparedStatement get_by_pmid = con.prepareStatement(q);
				for(Integer pmid : pmids){
					get_by_pmid.clearParameters();
					get_by_pmid.setString(1,pmid+"");
					ResultSet rslt = get_by_pmid.executeQuery();
					PubmedDoc pdoc = new PubmedDoc();
					pdoc.PMID = pmid;
					pdoc.triples = new HashSet<String>();
					while(rslt.next()){
						pdoc.triples.add(rslt.getString("SUBJECT_TEXT")+"_"+rslt.getString("PREDICATION.PREDICATE")+"_"+rslt.getString("OBJECT_TEXT"));
					}
					pdocs.add(pdoc);
				}
				get_by_pmid.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return pdocs;
	}


	public List<PubmedDoc> getSemPredicationsForPMIDList(List<Integer> pmids){
		List<PubmedDoc> pdocs = new ArrayList<PubmedDoc>(pmids.size());
		try {
			String r_query = "select umls.PRED_DEFINITIONS.DES, PREDICATION.*, SENTENCE_PREDICATION.*, SENTENCE.* "
					+ "from PREDICATION "
					+ "INNER join SENTENCE_PREDICATION on PREDICATION.PREDICATION_ID = SENTENCE_PREDICATION.PREDICATION_ID "
					+ "INNER JOIN SENTENCE ON SENTENCE.SENTENCE_ID = SENTENCE_PREDICATION.SENTENCE_ID "
					+ "INNER JOIN umls.PRED_DEFINITIONS ON umls.PRED_DEFINITIONS.NAME = PREDICATION.PREDICATE "
					+  " where SENTENCE.PMID = ? ";	

			PreparedStatement get_relations = con.prepareStatement(r_query);
			//get broad definitions
			String stype_def_query = "select * from "
					+ "PREDICATION_ARGUMENT, CONCEPT_SEMTYPE, CONCEPT, umls.TYPE_DEFINITIONS "
					+ "where PREDICATION_ID = ? "
					+ "AND PREDICATION_ARGUMENT.CONCEPT_SEMTYPE_ID = CONCEPT_SEMTYPE.CONCEPT_SEMTYPE_ID "
					+ "AND CONCEPT_SEMTYPE.CONCEPT_ID = CONCEPT.CONCEPT_ID "
					+ "AND CONCEPT_SEMTYPE.SEMTYPE = umls.TYPE_DEFINITIONS.ABBR ";
			PreparedStatement get_stype_defs = con.prepareStatement(stype_def_query);

			for(Integer pmid : pmids){
				get_relations.clearParameters();
				get_relations.setString(1,  pmid+"");
				ResultSet relations = get_relations.executeQuery();
				PubmedDoc pdoc = new PubmedDoc();
				pdoc.PMID = pmid;
				pdoc.predications = new ArrayList<SemRepPredication>();

				while(relations.next()){
					SemRepPredication srp = new SemRepPredication();
					//ID FOR PREDICATION
					int predication_id = relations.getInt("PREDICATION.PREDICATION_ID");
					srp.predication_id = predication_id;
					//text
					String sentence = relations.getString("SENTENCE");
					srp.sentence = sentence;
					srp.pmid = pmid;
					//int pmid = relations.getInt("PMID");
					//predicate
					String predicate = relations.getString("PREDICATE");
					srp.predicate = predicate;
					//predicate meaning
					String predicate_definition = relations.getString("PRED_DEFINITIONS.DES");
					srp.predicate_definition = predicate_definition;
					//subject TEXT
					String subject_text = relations.getString("SUBJECT_TEXT");
					srp.subject_text = subject_text;
					int subject_start_index = relations.getInt("SUBJECT_START_INDEX");
					srp.subject_start_index = subject_start_index - 1;  //makes it consistent with the gold standard..
					int subject_end_index = relations.getInt("SUBJECT_END_INDEX");
					srp.subject_end_index = subject_end_index;
					int subject_score = relations.getInt("SUBJECT_SCORE");				
					srp.subject_score = subject_score;
					//predicate
					String indicator_type = relations.getString("INDICATOR_TYPE");
					srp.indicator_type = indicator_type;
					int predicate_start_index = relations.getInt("PREDICATE_START_INDEX");
					srp.predicate_start_index = predicate_start_index;
					int predicate_end_index = relations.getInt("PREDICATE_END_INDEX");
					srp.predicate_end_index = predicate_end_index;
					//object
					String object_text = relations.getString("OBJECT_TEXT");
					srp.object_text = object_text;
					int object_start_index = relations.getInt("OBJECT_START_INDEX");
					srp.object_start_index = object_start_index - 1;
					int object_end_index = relations.getInt("OBJECT_END_INDEX");
					srp.object_end_index = object_end_index;
					int object_score = relations.getInt("OBJECT_SCORE");
					srp.object_score = object_score;
					//debug
					//	System.out.println(subject_text+"\t"+predicate+"\t"+predicate_definition+"\t"+object_text);				
					//now get the definitions for the terms if needed.  (optimization opportunity here)
					get_stype_defs.clearParameters();
					get_stype_defs.setInt(1, predication_id);
					ResultSet sdefs = get_stype_defs.executeQuery();
					String subject_CUI = ""; String subject_stype_name  = ""; String subject_stype_desc  = ""; String subject_desc  = ""; String subject_pref_name = "";
					String object_CUI = ""; String object_stype_name  = ""; String object_stype_desc  = ""; String object_desc  = ""; String object_pref_name = "";
					while(sdefs.next()){
						String sub_or_ob = sdefs.getString("TYPE"); 
						String CUI = sdefs.getString("CUI"); 
						String concept_preferred_name = sdefs.getString("PREFERRED_NAME");
						String stype_abbr = sdefs.getString("ABBR");
						String stype_name = sdefs.getString("NAME");
						String stype_desc = sdefs.getString("DES");
						//		System.out.println("\t"+sub_or_ob+"\t"+CUI+"\t"+concept_preferred_name+"\t"+stype_name);
						//TODO check if multiple
						List<SemanticType> s_stypes = new ArrayList<SemanticType>();
						SemanticType st = new SemanticType();
						if(sub_or_ob.equals("S")){
							srp.subject_CUI = CUI;
							srp.subject_pref_name = concept_preferred_name;
							st.abbr = stype_abbr;
							st.name = stype_name;
							st.desc = stype_desc;
							s_stypes.add(st);
							srp.subject_types = s_stypes;
						}else{
							srp.object_CUI = CUI;
							srp.object_pref_name = concept_preferred_name;
							st.abbr = stype_abbr;
							st.name = stype_name;
							st.desc = stype_desc;
							s_stypes.add(st);
							srp.object_types = s_stypes;
						}

						//term defs			
						//add definitions from umls
						String def = "undefined";
						def = pickOneDef(getCUIdefs(CUI));
						if(sub_or_ob.equals("S")){
							srp.subject_desc  = def;
						}else{
							srp.object_desc  = def;
						}

					}
					pdoc.predications.add(srp);
				}
				pdocs.add(pdoc);
				relations.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		return pdocs;
	}

	public void exportForVerification(String outfile){
		try {
			//get some random relations

			String r_query = "select umls.PRED_DEFINITIONS.DES, PREDICATION.*, SENTENCE_PREDICATION.*, SENTENCE.* "
					+ "from PREDICATION "
					+ "INNER join SENTENCE_PREDICATION on PREDICATION.PREDICATION_ID = SENTENCE_PREDICATION.PREDICATION_ID "
					+ "INNER JOIN SENTENCE ON SENTENCE.SENTENCE_ID = SENTENCE_PREDICATION.SENTENCE_ID "
					+ "INNER JOIN umls.PRED_DEFINITIONS ON umls.PRED_DEFINITIONS.NAME = PREDICATION.PREDICATE "
					//	+ "order by rand() "
					+  " where PREDICATION.PREDICATION_ID = 704116 "
					+ "limit 1000";			

			PreparedStatement get_relations = con.prepareStatement(r_query);
			//get broad definitions
			String stype_def_query = "select * from "
					+ "PREDICATION_ARGUMENT, CONCEPT_SEMTYPE, CONCEPT, umls.TYPE_DEFINITIONS "
					+ "where PREDICATION_ARGUMENT.PREDICATION_ID = ? "
					+ "AND PREDICATION_ARGUMENT.CONCEPT_SEMTYPE_ID = CONCEPT_SEMTYPE.CONCEPT_SEMTYPE_ID "
					+ "AND CONCEPT_SEMTYPE.CONCEPT_ID = CONCEPT.CONCEPT_ID "
					+ "AND CONCEPT_SEMTYPE.SEMTYPE = umls.TYPE_DEFINITIONS.ABBR ";
			PreparedStatement get_stype_defs = con.prepareStatement(stype_def_query);
			//get specific definitions
			//multiple definitions for each term..
			String def_query = "select * from umls.MRDEF where CUI = ?";
			PreparedStatement get_defs = con.prepareStatement(def_query);


			ResultSet relations = get_relations.executeQuery();

			String header = "predication_id\tindicator_type\tpredicate_start_index\tpredicate_end_index\tpredicate\tpredicate_definition";
			header+= "\tsubject_start_index\tsubject_end_index\tsubject_score\tsubject_text\tsubject_pref_name\tsubject_CUI\tsubject_stype_name\tsubject_stype_desc\tsubject_desc";
			header+="\tobject_start_index\tobject_end_index\tobject_score\tobject_text\tobject_pref_name\tobject_CUI\tobject_stype_name\tobject_stype_desc\tobject_desc";
			header+="\tsentence\tpmid";
			System.out.println(header);
			FileWriter writer = new FileWriter(outfile);
			writer.write(header+"\n");
			while(relations.next()){
				//ID FOR PREDICATION
				int predication_id = relations.getInt("PREDICATION.PREDICATION_ID");
				//text
				String sentence = relations.getString("SENTENCE");
				int pmid = relations.getInt("PMID");
				//predicate
				String predicate = relations.getString("PREDICATE");
				//predicate meaning
				String predicate_definition = relations.getString("PRED_DEFINITIONS.DES");
				//subject TEXT
				String subject_text = relations.getString("SUBJECT_TEXT");
				int subject_start_index = relations.getInt("SUBJECT_START_INDEX");
				int subject_end_index = relations.getInt("SUBJECT_END_INDEX");
				int subject_score = relations.getInt("SUBJECT_SCORE");				
				//predicate
				String indicator_type = relations.getString("INDICATOR_TYPE");
				int predicate_start_index = relations.getInt("PREDICATE_START_INDEX");
				int predicate_end_index = relations.getInt("PREDICATE_END_INDEX");
				//object
				String object_text = relations.getString("OBJECT_TEXT");
				int object_start_index = relations.getInt("OBJECT_START_INDEX");
				int object_end_index = relations.getInt("OBJECT_END_INDEX");
				int object_score = relations.getInt("OBJECT_SCORE");

				//debug
				//	System.out.println(subject_text+"\t"+predicate+"\t"+predicate_definition+"\t"+object_text);				
				//now get the definitions for the terms if needed.  (optimization opportunity here)
				get_stype_defs.clearParameters();
				get_stype_defs.setInt(1, predication_id);
				ResultSet sdefs = get_stype_defs.executeQuery();
				String subject_CUI = ""; String subject_stype_name  = ""; String subject_stype_desc  = ""; String subject_desc  = ""; String subject_pref_name = "";
				String object_CUI = ""; String object_stype_name  = ""; String object_stype_desc  = ""; String object_desc  = ""; String object_pref_name = "";
				while(sdefs.next()){
					String sub_or_ob = sdefs.getString("TYPE"); 
					String CUI = sdefs.getString("CUI"); 
					String concept_preferred_name = sdefs.getString("PREFERRED_NAME");
					String stype_abbr = sdefs.getString("ABBR");
					String stype_name = sdefs.getString("NAME");
					String stype_desc = sdefs.getString("DES");
					//		System.out.println("\t"+sub_or_ob+"\t"+CUI+"\t"+concept_preferred_name+"\t"+stype_name);

					if(sub_or_ob.equals("S")){
						subject_CUI = CUI;
						subject_stype_name = stype_name;
						subject_stype_desc = stype_desc;
						subject_pref_name = concept_preferred_name;
					}else{
						object_CUI = CUI;
						object_stype_name = stype_name;
						object_stype_desc = stype_desc;
						object_pref_name = concept_preferred_name;
					}

					//term defs			
					get_defs.clearParameters();
					get_defs.setString(1, CUI);
					ResultSet term_defs = get_defs.executeQuery();
					//just randomly picks the first one..
					if(term_defs.next()){
						String source = term_defs.getString("SAB");
						String def = term_defs.getString("DEF");
						if(sub_or_ob.equals("S")){
							subject_desc = def;
						}else{
							object_desc = def;
						}
						//		System.out.println("\t\t"+source+"\t"+def);
					}
				}
				String row = predication_id+"\t"+indicator_type+"\t"+predicate_start_index+"\t"+predicate_end_index+"\t"+predicate+"\t"+predicate_definition;
				row+= "\t"+subject_start_index+"\t"+subject_end_index+"\t"+subject_score+"\t"+subject_text+"\t"+subject_pref_name+"\t"+subject_CUI+"\t"+subject_stype_name+"\t"+subject_stype_desc+"\t"+subject_desc;
				row+="\t"+object_start_index+"\t"+object_end_index+"\t"+object_score+"\t"+object_text+"\t"+object_pref_name+"\t"+object_CUI+"\t"+object_stype_name+"\t"+object_stype_desc+"\t"+object_desc;
				row+="\t"+sentence+"\t"+pmid;
				System.out.println(row);
				writer.write(row+"\n");
			}
			writer.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

/**
 * Return a set of CUIs used in SemmedDB. Uses 'used_cuis' table which is a simple list table extracted from SemmedDB for convenience.
 * If unmapped=true, check cui_qid and remove any that are already present.
 * Used to iteratively build up the mapping table.
 * Orderby's helpful for dirt simple parallelization (running multiple processes starting from the beginning, end or middle (rand) of the list. 
 * @param limit
 * @param unmapped
 * @param orderby [desc, asc, rand()]
 * @return
 */
	public Set<String> getUnMappedCUIsInSemmedDB(int limit, boolean unmapped, String orderby){
		Set<String> cuis = new HashSet<String>();
		try {
			String q ="select distinct cui "
					+ "from used_cuis "
					+ "where cui like 'c%' "  //filters out entrez ids
					+ "and cui not like '%|%' "; //filters out things like 10003|219595 or C0000084|64663
			if(unmapped){		
					q+= " and not exists (select cui from cui_qid where cui_qid.cui = used_cuis.cui) "; //removes any we have already tried to map
			}		
			if(orderby.equals("desc")||orderby.equals("asc")||orderby.equals("rand()")){
				q+="order by cui  "; 
			}
			q+="limit "+limit;
			PreparedStatement getcuis = con.prepareStatement(q);
			ResultSet rslt = getcuis.executeQuery();
			while(rslt.next()){
				cuis.add(rslt.getString("cui"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return cuis;
	}

	
	public void extractPredsDb(String prop, int limit, String outfile) throws IOException{
		UmlsWikidataBridge b = new UmlsWikidataBridge();
		//load precomputed mapping
		System.out.println("Loading cui_qui map");
		b.loadStoredMapFromDatabase(null, 10000000);//no limit
		System.out.println("Loaded "+b.cui_items.size()+" cui maps ");
		FileWriter predicates_out = new FileWriter(outfile);
		String pred_header = "subject_qid	property_id	object_qid	reference_uri	reference_supporting_text	reference_date";
		predicates_out.write(pred_header+"\n");
		int n_total = 0;
		int n_stored = 0;
		try {
			//non streaming - all results in memory
			//PreparedStatement get_causes = con.prepareStatement();	
			//streaming
			java.sql.Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
		              java.sql.ResultSet.CONCUR_READ_ONLY);
			stmt.setFetchSize(Integer.MIN_VALUE);
			String query = "select PREDICATION_AGGREGATE.*, SENTENCE.SENTENCE,CITATIONS.EDAT "+
			"from  PREDICATION_AGGREGATE, SENTENCE, CITATIONS  "+  
			"where PREDICATION_AGGREGATE.predicate = '"+prop+"' ";
			if(prop.equals("ASSOCIATED_WITH")){
				query+= " and (s_type = 'aapp' or s_type = 'gngm') "; //wikidata property used is tied to genes
			}			
			query+="and PREDICATION_AGGREGATE.SID = SENTENCE.SENTENCE_ID "+ 
			"and PREDICATION_AGGREGATE.PMID = CITATIONS.PMID "+
			"and SENTENCE.PMID = CITATIONS.PMID "+
			"limit "+limit;
						
			System.out.println("Querying for triples ");
			ResultSet relations = stmt.executeQuery(query);
			System.out.println("Exporting triples ");
			while(relations.next()){
				SemRepPredication srp = new SemRepPredication();
				//ID FOR PREDICATION
				int predication_id = relations.getInt("PID");
				srp.predication_id = predication_id;
				//text
				int sentence_id = relations.getInt("SID");
				srp.sentence_id = sentence_id;
				srp.sentence = relations.getString("SENTENCE.SENTENCE");
				srp.pmid = relations.getInt("PMID");;
				//Date pdate = 
				String edat = relations.getString("CITATIONS.EDAT");
				srp.pubdate = Calendar.getInstance();
				if(isValidDate(edat)){
					srp.pubdate.setTime(relations.getDate("CITATIONS.EDAT"));
				}else{
					srp.pubdate = null;
				}
				//int pmid = relations.getInt("PMID");
				//predicate
				String predicate = relations.getString("predicate");
				srp.predicate = predicate;
				//predicate meaning
				//String predicate_definition = relations.getString("PRED_DEFINITIONS.DES");
				srp.predicate_definition = "";
				//subject TEXT
				String subject_text = relations.getString("s_name");
				srp.subject_text = subject_text;
				srp.subject_CUI = relations.getString("s_cui");
				//object
				String object_text = relations.getString("o_name");
				srp.object_text = object_text;
				srp.object_CUI = relations.getString("o_cui");
				//types 
				//just adding groups in here.  this will most likely cause a bug //TODO fix
				SemanticType subject_type = new SemanticType();
				subject_type.abbr = relations.getString("s_type");
				SemanticType object_type = new SemanticType();
				object_type.abbr = relations.getString("o_type");
				srp.subject_types.add(subject_type);
				srp.object_types.add(object_type);
				
				if(b.cui_items.get(srp.subject_CUI)!=null&&b.cui_items.get(srp.object_CUI)!=null){
					//only allow 1-1 mappings into this one
					if(b.cui_items.get(srp.subject_CUI).size()==1&&b.cui_items.get(srp.object_CUI).size()==1){
						String subject_item = b.cui_items.get(srp.subject_CUI).iterator().next();
						String object_item = b.cui_items.get(srp.object_CUI).iterator().next();
						String property = srp.predicate;
						String link = "https://www.ncbi.nlm.nih.gov/pubmed/"+srp.pmid;
						String property_uri = b.prop_map.get(property);
						String sentence = srp.sentence;
						Calendar cal = srp.pubdate;
						String date = "not set";
						if(cal!=null){
							SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
							date = df.format(cal.getTime());
						}
						String predicate_row = subject_item+"\t"+property_uri+"\t"+object_item+"\t"+link+"\t"+sentence+"\t"+date;
						//System.out.println(row);
						predicates_out.write(predicate_row+"\n");
						n_stored++;
					}
				}
				n_total++;
				if(n_total%1000==0){
					System.out.println("Read "+n_total+" saved "+n_stored);
				}
			}
			relations.close();
	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		predicates_out.close();
		System.out.println("saved "+n_stored+" triples of "+n_total);
		return;
		
	}
	
	
	/*causals: 625886
	prevents: 110324
	causes and prevents: 15832 */
	public List<SemRepPredication> getByProp(String prop, int limit){
		List<SemRepPredication> preds = new ArrayList<SemRepPredication>();
		try {
			PreparedStatement get_causes = con.prepareStatement(
					//+ "select * from PREDICATION_AGGREGATE where predicate = 'CAUSES' OR predicate = 'PREVENTS' limit "+limit);
			"select PREDICATION_AGGREGATE.*, SENTENCE.SENTENCE,CITATIONS.EDAT "+
			"from  PREDICATION_AGGREGATE, SENTENCE, CITATIONS  "+  
			"where PREDICATION_AGGREGATE.predicate = '"+prop+"' " + 
			"and PREDICATION_AGGREGATE.SID = SENTENCE.SENTENCE_ID "+ 
			"and PREDICATION_AGGREGATE.PMID = CITATIONS.PMID "+
			"and SENTENCE.PMID = CITATIONS.PMID "+
			"limit "+limit);
					
/**
| PID    | SID    | PNUMBER | PMID     | predicate | s_cui    | s_name                     | s_type | s_novel | o_cui    | o_name                 | o_type | o_novel |
+--------+--------+---------+----------+-----------+----------+----------------------------+--------+---------+----------+------------------------+--------+---------+
| 104259 | 285772 |       1 | 16691656 | CAUSES    | C0029385 | Osmium Tetroxide           | hops   |       1 | C0002418 | Amblyopia              | dsyn   |       1 |
 */
		//	System.out.println(get_causes);
			
			ResultSet relations = get_causes.executeQuery();
			while(relations.next()){
				SemRepPredication srp = new SemRepPredication();
				//ID FOR PREDICATION
				int predication_id = relations.getInt("PID");
				srp.predication_id = predication_id;
				//text
				int sentence_id = relations.getInt("SID");
				srp.sentence_id = sentence_id;
				srp.sentence = relations.getString("SENTENCE.SENTENCE");
				srp.pmid = relations.getInt("PMID");;
				//Date pdate = 
				String edat = relations.getString("CITATIONS.EDAT");
				srp.pubdate = Calendar.getInstance();
				if(isValidDate(edat)){
					srp.pubdate.setTime(relations.getDate("CITATIONS.EDAT"));
				}else{
					srp.pubdate = null;
				}
				//int pmid = relations.getInt("PMID");
				//predicate
				String predicate = relations.getString("predicate");
				srp.predicate = predicate;
				//predicate meaning
				//String predicate_definition = relations.getString("PRED_DEFINITIONS.DES");
				srp.predicate_definition = "";
				//subject TEXT
				String subject_text = relations.getString("s_name");
				srp.subject_text = subject_text;
				srp.subject_CUI = relations.getString("s_cui");
				//object
				String object_text = relations.getString("o_name");
				srp.object_text = object_text;
				srp.object_CUI = relations.getString("o_cui");
				//types 
				//just adding groups in here.  this will most likely cause a bug //TODO fix
				SemanticType subject_type = new SemanticType();
				subject_type.abbr = relations.getString("s_type");
				SemanticType object_type = new SemanticType();
				object_type.abbr = relations.getString("o_type");
				srp.subject_types.add(subject_type);
				srp.object_types.add(object_type);
				preds.add(srp);
			}
			System.out.println("cause and prevent: "+preds.size());
			relations.close();
	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return preds;
	}

	
	public static boolean isValidDate(String inDate) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy MM dd");//2006 06 01
	    dateFormat.setLenient(false);
	    try {
	      dateFormat.parse(inDate.trim());
	    } catch (ParseException pe) {
	      return false;
	    }
	    return true;
	  }
	
	public Map<String, String> getCUIdefs(String CUI) throws SQLException{
		get_cui_defs.clearParameters();
		get_cui_defs.setString(1, CUI);
		ResultSet def_rslts = get_cui_defs.executeQuery();
		Map<String, String> defs = new HashMap<String, String>();
		while(def_rslts.next()){
			String source = def_rslts.getString("SAB");
			//good NCI_NCI-GLOSS NCI MSH CSP
			//bad MEDLINEPLUS
			defs.put(source, def_rslts.getString("DEF"));
		}
		def_rslts.close();
		return defs;
	}

	public String pickOneDef(Map<String, String> source_def){
		String def = "";
		for(String source : source_def.keySet()){
			def = source_def.get(source);
			//these are good
			if(source.equals("NCI_NCI-GLOSS")){
				return def;
			}else if(source.equals("NCI")){
				return def;
			}else if(source.equals("MSH")){
				return def;
			}else if(source.equals("CSP")){
				def = source_def.get(source);
				return def;
			}
		}
		//but at least return something..
		return def;
	}

}
