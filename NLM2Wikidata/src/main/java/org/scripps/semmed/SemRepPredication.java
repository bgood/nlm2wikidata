/**
 * 
 */
package org.scripps.semmed;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.scripps.umls.SemanticType;

/**
 * @author bgood
 *
 */
public class SemRepPredication {
	// PREDICATION
	public String source;
	public int predication_id;
	//text
	public String section; //title or abstract
	public String sentence;
	public int sentence_id;
	public int pmid;
	public Calendar pubdate;
	//predicate
	public String predicate;
	public String predicate_text;
	//predicate meaning
	public String predicate_definition;
	//subject TEXT
	public String subject_text;
	public int subject_start_index;
	public int subject_end_index;
	public int subject_score;				
	//predicate
	public String indicator_type;
	public int predicate_start_index;
	public int predicate_end_index;
	//object
	public String object_text;
	public int object_start_index;
	public int object_end_index;
	public int object_score;
	//concepts
	public String subject_CUI; 
	public List<SemanticType> subject_types;
	public String subject_desc; 
	public String subject_pref_name;
	public String object_CUI; 
	public List<SemanticType> object_types;
	public String object_desc; 
	public String object_pref_name;

	public SemRepPredication(){
		subject_types = new ArrayList<SemanticType>();
		object_types = new ArrayList<SemanticType>();
	}


	/**
	 * use the semantic type information to give some idea at least..
	 */
	public void improveOnUndefined(){
		if(subject_desc==null||subject_desc.equals("undefined")){
			if(subject_types==null){
				return;
			}
			for(SemanticType t : subject_types){
				String name = t.name;
				if(name.equals("Gene or Genome")){  
					subject_desc = t.name+". "+t.desc;
					break;
				}else{
					subject_desc = t.name+". "+t.desc;
				}
			}
		}
		if(object_desc==null||object_desc.equals("undefined")){
			if(object_types==null){
				return;
			}
			for(SemanticType t : object_types){
				String name = t.name;
				if(name.equals("Gene or Genome")){  
					object_desc = t.name+". "+t.desc;
					break;
				}else{
					object_desc = t.name+". "+t.desc;
				}
			}
		}
	}

	public String getRow(){
		improveOnUndefined();

		String row = predication_id+"\t"+indicator_type+"\t"+predicate_start_index+"\t"+predicate_end_index+"\t"+predicate+"\t"+predicate_definition;
		row+= "\t"+subject_start_index+"\t"+subject_end_index+"\t"+subject_score+"\t"+subject_text+"\t"+subject_pref_name+"\t"+subject_CUI+"\t";
		for(SemanticType s : subject_types){
			row += s.name+",";
		}
		row = row.substring(0,row.length()-1)+"\t";
		for(SemanticType s : subject_types){
			row += s.desc+",";
		}
		row = row.substring(0,row.length()-1);
		row+="\t"+subject_desc;
		row+="\t"+object_start_index+"\t"+object_end_index+"\t"+object_score+"\t"+object_text+"\t"+object_pref_name+"\t"+object_CUI+"\t";
		for(SemanticType s : object_types){
			row += s.name+",";
		}
		row = row.substring(0,row.length()-1)+"\t";
		for(SemanticType s : object_types){
			row += s.desc+",";
		}
		row+="\t"+object_desc;		
		row+="\t"+sentence+"\t"+getSentenceHtml()+"\t"+pmid;
		return row;
	}

	public static String getHeader(){
		String header = "predication_id\tindicator_type\tpredicate_start_index\tpredicate_end_index\tpredicate\tpredicate_definition";
		header+= "\tsubject_start_index\tsubject_end_index\tsubject_score\tsubject_text\tsubject_pref_name\tsubject_CUI\tsubject_stype_name\tsubject_stype_desc\tsubject_desc";
		header+="\tobject_start_index\tobject_end_index\tobject_score\tobject_text\tobject_pref_name\tobject_CUI\tobject_stype_name\tobject_stype_desc\tobject_desc";
		header+="\tsentence\tsentence_html\tpmid";
		return header;
	}

	public String getSentenceHtml(){
		String s = sentence;

		if(subject_end_index<object_start_index){
			s = insertInString(s,"<strong>", "</strong>", subject_start_index, subject_end_index);
			int new_o_start = object_start_index+17;		
			int new_o_end = object_end_index+17;
			s = insertInString(s,"<strong>", "</strong>", new_o_start, new_o_end);
		}else if(object_end_index<subject_start_index){
			s = insertInString(s,"<strong>", "</strong>", object_start_index, object_end_index);
			int new_s_start = subject_start_index+17;		
			int new_s_end = subject_end_index+17;
			s = insertInString(s,"<strong>", "</strong>", new_s_start, new_s_end);
		}//TODO overlaps too complicated for now.. fix it with a better reg that doesn't break on special chars maybe.
		//		}else{
		//			s = s.replaceAll(subject_text, "<strong>"+subject_text+"</strong>");
		//			s = s.replaceAll(object_text, "<strong>"+object_text+"</strong>");
		//		}

		String html = "<div>"+s+"</div>";
		return html;
	}

	public String insertInString(String s, String prefix, String postfix, int start_position, int end_position){
		String out = s.substring(0,start_position)+prefix+s.substring(start_position);
		int endp = end_position+prefix.length();
		out = out.substring(0, endp)+postfix+out.substring(endp);
		return out;
	}
/*
	public void populateFromFile(String pred_file){
		BufferedReader f = new BufferedReader(new FileReader(pred_file));
		String line = f.readLine();
		line = f.readLine();//skip header
		while(line!=null){
			String[] item = line.split("\t");
			predication_id= Integer.parseInt(item[0]);
			predicate_start_index= Integer.parseInt(item[1]);
			predicate_end_index= Integer.parseInt(item[2]);
			predicate= item[3];
			predicate_definition = item[4];
			subject_start_index= Integer.parseInt(item[5]);
			subject_end_index= Integer.parseInt(item[6]);
			subject_score= Integer.parseInt(item[7]);
			subject_text= item[8];
			subject_pref_name= item[9];
			subject_CUI= item[10];
	//		subject_stype_name = item[11];
	//		subject_stype_desc = item[12];
			subject_desc= item[13];
			object_start_index= Integer.parseInt(item[14]);
			object_end_index= Integer.parseInt(item[15]);
			object_score= Integer.parseInt(item[16]);
			object_text= item[17];
			object_pref_name= item[18];
			object_CUI= item[19];
	//		object_stype_name= item[20];
	//		object_stype_desc= item[21];
			object_desc= item[22];
			sentence= item[23];
	//		sentence_html= item[24];
			pmid = Integer.parseInt(item[25]);
		}
	f.close();
	
}
*/
	
}
