package org.scripps.semmed;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.scripps.umls.SemanticType;

public class SemRepUtility {

	public static void main(String[] args) throws SQLException {
		//compareSemrepOutputToGold(false);
		//gold pmids: 308
		//Matching pmids from semrep: 308
		//		System.out.println(tp+"\t"+fp+"\t"+fn+"\t"+precision+"\t"+recall+"\t"+f);
		//442	1687	859	0.2076092	0.33973867	0.25772592

		//ignoring relation type and subject object order
		//sentence level
		//499	3537	789	0.12363727	0.38742235	0.18745305

		//overlap at co-occurrence level text
		//626	27726	662	0.02207957	0.48602486	0.042240214
		//overlap at co-occurrence level cuis
		//561	21407	695	0.025537144	0.44665605	0.04831209

		//			SemRepXML sr = new SemRepXML();
		//			List<PubmedDoc> gold_docs = sr.getGoldPredications();
		//			compareSemrepCoccurOutputToGold(gold_docs, false, true, true);


		//		String file = "data/gold_gdd_annos/EUADR_Corpus_IBIgroup/EUADR_target_disease.csv";
		//		List<PubmedDoc> gold_docs = EuadrProcessor.getEuadrDocs(file);
		//		compareSemrepCoccurOutputToGold(gold_docs, false, false, false);

		String cf = "data/semmed_verify/dev_functional_rels_CF_output.txt";
		List<PubmedDoc> cf_docs = loadTabPredicationsFile(cf, true, false);
		String gold = "data/semmed_verify/dev_functional_rels_gold.txt";
		List<PubmedDoc> gold_docs = loadTabPredicationsFile(gold, false, true);
		List<String> allowed_predicates = new ArrayList<String>();
		allowed_predicates.add("CAUSES");
		allowed_predicates.add("INHIBITS");
		allowed_predicates.add("AFFECTS");
		allowed_predicates.add("INTERACTS_WITH");
		allowed_predicates.add("PREDISPOSES");
		allowed_predicates.add("AFFECTS");
		allowed_predicates.add("AUGMENTS");
		allowed_predicates.add("DISRUPTS");
		compare(cf_docs, gold_docs, true, allowed_predicates);

		//(tp+"\t"+fp+"\t"+fn+"\t"+precision+"\t"+recall+"\t"+f);		
		//	tp	fp	fn	P	R	F
		//limited to functional 
		//15 pmids
		//removed 15 assertions
		//majority_cf	6	24	27	0.2	0.18181819	0.19047621	
		//strict_cf	6	21	27	0.22222222	0.18181819	0.2
		
		
		//keep_all	7	38	26	0.15555556	0.21212122	0.17948718
		
		/*
		 
		 SemRep predictions as compared to gold standard
		 true positive	7
		 false positive	38
		 false negative 27
		 P 0.16	R 0.21	F 0.18
		 
		 If CF used to as a filter we get
		 
		 true positive	6
		 false positive	21
		 
		 false negative 27
		 P 0.2	R 0.18	F 0.19
		 
		 Is that significant?
		 
		 Cf judged 19 out of the 45 SemRep predictions to be False positives
		 18 of them were correct.  
		 
		 The initial false positive rate for SemRep was 38/45 = 0.84
		 
		 */
		
		
		//5	40	16
		/*
		 Out of 48 predicted relations, CF workers (5 at 5c/task, majority rule),
		 filtered out 19 predictions.  
		 As compared to the gold standard, 18 of those were false positives giving the crowd

		 crowd correctly filtered = 18 (tp) - specificity 18/19 = 0.95
		 crowd incorrectly filtered = 1 (fp) - 
		 crowd should have filtered = 21 (fn) - sensitivity 19 / (18 + 21) = 0.49
		 
		 */
		
		System.out.println(SemRepPredication.getHeader());
		
	}

	/**
 0	_unit_id
1	_golden	TRUE/FALSE
2	_unit_state
3	_trusted_judgments
4	_last_judgment_at
5	any_comments
6	category No Yes
7	category:confidence 0.627 0.6013
8	category_gold
9	indicator_type
10	object_cui
11	object_desc
12	object_end_index
13	object_pref_name
14	object_score
15	object_start_index
16	object_stype_desc
17	object_stype_name
18	object_text
19	pmid
20	predicate
21	predicate_definition
22	predicate_end_index
23	predicate_start_index
24	predication_id
25	sentence
26	sentence_html
27	subject_cui
28	subject_desc
29	subject_end_index
30	subject_pref_name
31	subject_score
32	subject_start_index
33	subject_stype_desc
34	subject_stype_name
35	subject_text
	 * @param input_file
	 * @return
	 */
	public static List<PubmedDoc> loadTabPredicationsFile(String input_file, boolean cf, boolean random){
		Map<Integer, PubmedDoc> pmid_doc = new HashMap<Integer, PubmedDoc>();
		int n_keep = 0; int n_drop = 0;
		try {
			Reader in = new FileReader(input_file);
			Iterable<CSVRecord> records = CSVFormat.TDF.withHeader().parse(in);
			for (CSVRecord record : records) {
				SemRepPredication pred = new SemRepPredication();
				Integer pmid = Integer.parseInt(record.get("pmid"));
				pred.pmid = pmid;
				pred.object_CUI = record.get("object_cui");
				pred.object_text = record.get("object_text");
				pred.object_pref_name = record.get("object_pref_name");
				pred.predicate = record.get("predicate");
				pred.subject_CUI = record.get("subject_cui");
				pred.subject_text = record.get("subject_text");
				pred.subject_pref_name = record.get("subject_pref_name");
				pred.sentence = record.get("sentence");


				String category = "Yes";//
				if(cf){//check if crowd keeps it
					//skip gold
					if(record.get("_golden").equals("TRUE")){
						continue;
					}
					String categories = record.get("category");
					String[] cats = categories.split("\\n");
					if(cats.length>1){
						category = "No";
//						String[] cat_confs = record.get("category:confidence").split("\\n");
//						float cat1_conf = Float.parseFloat(cat_confs[0]);
//						float cat2_conf = Float.parseFloat(cat_confs[1]);
//						if(cat1_conf>cat2_conf){
//							category = cats[0];
//						}else{
//							category = cats[1];
//						}
					}else{
						category = cats[0];
					}
				}
//				if(random){
//					if(Math.random()>0.5){
//						category = "No";
//					}
//				}
				if(category.equals("Yes")){
					//keep it
					PubmedDoc doc = pmid_doc.get(pmid);
					if(doc==null){
						doc = new PubmedDoc();
					}
					if(doc.predications==null){
						doc.predications = new ArrayList<SemRepPredication>();
					}
					doc.predications.add(pred);
					pmid_doc.put(pmid, doc);
					n_keep++;
				}else{
					//filter it out
					System.out.println(n_drop+"\tdrop "+"\t"+pred.getRow());
					n_drop++;
				}
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("keep "+"\t"+n_keep+"\tdrop\t"+n_drop);
		return new ArrayList<PubmedDoc>(pmid_doc.values());
	}




	/**
	 * Regardless of predication, see what level of recall could be had based on the concepts detected.  
	 * @param cuis
	 * @throws SQLException 
	 */
	public static void compareSemrepCoccurOutputToGold(List<PubmedDoc> gold_docs, boolean cuis, boolean all_order, boolean all_abstract) throws SQLException{
		List<Integer> pmids = new ArrayList<Integer>();
		Set<String> all_gold_tuples = new HashSet<String>();
		Set<String> gold_concepts = new HashSet<String>();
		for(PubmedDoc doc : gold_docs){
			pmids.add(doc.PMID);
			for(SemRepPredication pred : doc.predications){
				if(cuis){
					all_gold_tuples.add(pred.subject_CUI+"_"+pred.object_CUI);
					gold_concepts.add(pred.subject_CUI); gold_concepts.add(pred.object_CUI);
				}else{
					all_gold_tuples.add(pred.subject_text+"_"+pred.object_text);
					gold_concepts.add(pred.subject_text); gold_concepts.add(pred.object_text);
				}
			}
		}

		System.out.println("gold pmids: "+pmids.size());
		SemDb d = new SemDb();
		List<PubmedDoc> semrep_data = d.getSemPredicationsForPMIDList(pmids);
		System.out.println("Matching pmids from semrep: "+semrep_data.size());
		Set<String> all_semrep_tuples = new HashSet<String>();
		Set<String> semrep_concepts = new HashSet<String>();
		for(PubmedDoc doc : semrep_data){
			//assemble all by all for each pmid.. 
			if(all_abstract){
				Set<String> concepts = new HashSet<String>();
				for(SemRepPredication pred : doc.predications){
					if(cuis&&pred.subject_CUI!=null&&pred.object_CUI!=null){
						concepts.add(pred.subject_CUI);
						concepts.add(pred.object_CUI);
					}else{
						concepts.add(pred.subject_text);
						concepts.add(pred.object_text);
					}
				}
				for(String concept1 : concepts){
					for(String concept2 : concepts){
						if(!concept1.equals(concept2)){
							all_semrep_tuples.add(concept1+"_"+concept2);
						}
					}
				}
				semrep_concepts.addAll(concepts);
			}
			else{
				for(SemRepPredication pred : doc.predications){
					//					if(pred.subject_types.contains("aapp")||
					//							pred.subject_types.contains("gngm")){
					if(cuis){
						all_semrep_tuples.add(pred.subject_CUI+"_"+pred.object_CUI);
						if(all_order){
							all_semrep_tuples.add(pred.object_CUI+"_"+pred.subject_CUI);
						}
					}else{
						all_semrep_tuples.add(pred.subject_text+"_"+pred.object_text);
						System.out.print("semmed\t"+doc.PMID+"\t"+pred.subject_text+"\t"+pred.object_text+"\t");
						for(SemanticType t : pred.subject_types){
							System.out.print(t.abbr+",");
						}
						System.out.print("\t");
						for(SemanticType t : pred.object_types){
							System.out.print(t.abbr+",");
						}
						System.out.print("\t");
						if(all_gold_tuples.contains(pred.subject_text+"_"+pred.object_text)){
							System.out.print("tp\n");
						}else{
							System.out.print("fp\n");
						}


						if(all_order){
							all_semrep_tuples.add(pred.object_text+"_"+pred.subject_text);
						}
					}
				}
			}
		}
		int n_semrep_triples = all_semrep_tuples.size();
		int n_gold_triples = all_gold_tuples.size();
		all_semrep_tuples.retainAll(all_gold_tuples);
		int tp = all_semrep_tuples.size();
		int fp = n_semrep_triples - tp;
		int fn = n_gold_triples - tp;
		float precision = (float)tp/n_semrep_triples;
		float recall = (float)tp/n_gold_triples;
		float f = 2*precision*recall/(precision+recall);
		System.out.println("overlap at co-occurrence level");
		System.out.println(tp+"\t"+fp+"\t"+fn+"\t"+precision+"\t"+recall+"\t"+f);

		System.out.println("Distinct gold concepts: "+gold_concepts.size());
		System.out.println("Distinct semrep concepts: "+semrep_concepts.size());
		gold_concepts.removeAll(semrep_concepts);
		System.out.println("Gold concepts missing from semrep: "+gold_concepts.size());
		//		for(String c : gold_concepts){
		//			System.out.println(c);
		//		}
	}


	public static void compare(List<PubmedDoc> source_docs, List<PubmedDoc> gold_docs, boolean cuis, List<String>allowed_predicates){
		Set<String> all_gold_triples = new HashSet<String>();
		List<Integer> pmids = new ArrayList<Integer>();
		for(PubmedDoc doc : gold_docs){
			pmids.add(doc.PMID);
			if(doc.predications!=null){
				for(SemRepPredication srp: doc.predications){
					if(allowed_predicates==null||allowed_predicates.contains(srp.predicate)){
						String triple = srp.subject_CUI+srp.predicate+srp.object_CUI;
						all_gold_triples.add(triple);
					}
				}
			}
		}
		System.out.println("gold pmids: "+source_docs.size());
		Set<String> all_semrep_triples = new HashSet<String>();
		for(PubmedDoc doc : source_docs){
			if(pmids.contains(doc.PMID)){
				for(SemRepPredication srp: doc.predications){
					if(allowed_predicates==null||allowed_predicates.contains(srp.predicate)){
						String triple = srp.subject_CUI+srp.predicate+srp.object_CUI;
						all_semrep_triples.add(triple);
					}
				}
			}
		}
		int n_semrep_triples = all_semrep_triples.size();
		int n_gold_triples = all_gold_triples.size();
		all_semrep_triples.retainAll(all_gold_triples);
		int tp = all_semrep_triples.size();
		int fp = n_semrep_triples - tp;
		int fn = n_gold_triples - tp;
		float precision = (float)tp/n_semrep_triples;
		float recall = (float)tp/n_gold_triples;
		float f = 2*precision*recall/(precision+recall);

		System.out.println(tp+"\t"+fp+"\t"+fn+"\t"+precision+"\t"+recall+"\t"+f);
	}


	public static void compareSemrepOutputToGold(List<PubmedDoc> gold_docs, boolean cuis){
		List<Integer> pmids = new ArrayList<Integer>();
		Set<String> all_gold_triples = new HashSet<String>();
		for(PubmedDoc doc : gold_docs){
			pmids.add(doc.PMID);
			all_gold_triples.addAll(doc.triples);
		}
		System.out.println("gold pmids: "+pmids.size());
		SemDb d = new SemDb();
		List<PubmedDoc> semrep_data = d.getTriplesForPMIDList(pmids, cuis);
		System.out.println("Matching pmids from semrep: "+semrep_data.size());
		Set<String> all_semrep_triples = new HashSet<String>();
		for(PubmedDoc doc : semrep_data){
			all_semrep_triples.addAll(doc.triples);
		}
		int n_semrep_triples = all_semrep_triples.size();
		int n_gold_triples = all_gold_triples.size();
		all_semrep_triples.retainAll(all_gold_triples);
		int tp = all_semrep_triples.size();
		int fp = n_semrep_triples - tp;
		int fn = n_gold_triples - tp;
		float precision = (float)tp/n_semrep_triples;
		float recall = (float)tp/n_gold_triples;
		float f = 2*precision*recall/(precision+recall);

		System.out.println(tp+"\t"+fp+"\t"+fn+"\t"+precision+"\t"+recall+"\t"+f);
	}

	static public URL fileToURL(String sfile) 
	{
		File file = new File(sfile);
		String path = file.getAbsolutePath();
		String fSep = System.getProperty("file.separator");
		if (fSep != null && fSep.length() == 1)
			path = path.replace(fSep.charAt(0), '/');
		if (path.length() > 0 && path.charAt(0) != '/')
			path = '/' + path;
		try 
		{
			return new URL("file", null, path);
		}
		catch (java.net.MalformedURLException e) 
		{
			// According to the spec this could only happen if the file
			// protocol were not recognized. 
			throw new Error("unexpected MalformedURLException");
		}
	}

}
