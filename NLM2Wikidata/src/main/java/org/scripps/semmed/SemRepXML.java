/**
 * 
 */
package org.scripps.semmed;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.scripps.umls.SemanticType;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author bgood
 *
 */
public class SemRepXML {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public static void main(String args[]) throws IOException, SQLException {

		
		SemRepXML sr = new SemRepXML();
		List<PubmedDoc> gold = sr.getGoldPredications();
		//make a subset with 50 abstracts for development purposes
		//limit to ensure we have a definition for each term
		int limit = 50;
		FileWriter f = new FileWriter("data/semmed_verify/dev_gold.tsv");		
		f.write(SemRepPredication.getHeader()+"\n");
		Set<Integer> pmids = new HashSet<Integer>();
		Collections.shuffle(gold);
		for(PubmedDoc pdoc : gold){
			//first check that we have CUIs for all predications
			//e.g. there are several genes in there with no CUI
			boolean keep = true;
			for(SemRepPredication p : pdoc.predications){
				if(p.subject_CUI.equals("")||p.object_CUI.equals("")){
					keep=false;
					break;
				}
			}
			if(!keep){
				continue;
			}
			pmids.add(pdoc.PMID);
			for(SemRepPredication p : pdoc.predications){
				f.write(p.getRow()+"\n");
			}
			if(pmids.size()>=limit){
				break;
			}

		}
		f.close();
		//write out the SemRep predictions for these abstracts
		SemDb db = new SemDb();
		f = new FileWriter("data/semmed_verify/dev_semmed.tsv");
		f.write(SemRepPredication.getHeader()+"\n");
		List<PubmedDoc> predicted = db.getSemPredicationsForPMIDList(new ArrayList<Integer>(pmids));
		for(PubmedDoc pdoc : predicted){
			for(SemRepPredication p : pdoc.predications){
				f.write(p.getRow()+"\n");
			}
		}
		f.close();
		//compare
		SemRepUtility.compare(predicted, gold, true, null);
		
	}


	public List<PubmedDoc> getGoldPredications() throws SQLException{
		//for definitions
		SemDb db = new SemDb();

		int limit = 5000;
		List<PubmedDoc> docs = new ArrayList<PubmedDoc>();
		int n_triples = 0;  int ndocs = 0;
		try {

			File fXmlFile = new File("data/semmed_verify/adjudicated_gold.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("MedlineCitation");
			int nn = 0;
			for (int temp = 0; temp < nList.getLength(); temp++) {
				nn++;
				if(nn>limit){
					break;
				}
				Node medcite = nList.item(temp);
				Element eElement = (Element) medcite;
				PubmedDoc pdoc = new PubmedDoc();
				pdoc.PMID = Integer.parseInt(eElement.getAttribute("pmid"));
				pdoc.predications = new ArrayList<SemRepPredication>();
				NodeList sentences = eElement.getElementsByTagName("Sentence");
				ndocs++;
				for(int s = 0; s < sentences.getLength(); s++){
					Element sentence = (Element)sentences.item(s);
					String sentence_text = sentence.getAttribute("text");
					String section = sentence.getAttribute("section");
					NodeList predicationss = sentence.getElementsByTagName("Predications");
					for(int p = 0; p < predicationss.getLength(); p++){
						Element predication_node = (Element)predicationss.item(p);
						NodeList predications = predication_node.getElementsByTagName("Predication");
						for(int i=0; i<predications.getLength(); i++){
							SemRepPredication srp = new SemRepPredication();

							Element predication = (Element)predications.item(i);							
							boolean negated = Boolean.parseBoolean(predication.getAttribute("negated"));
							boolean inferred =Boolean.parseBoolean(predication.getAttribute("inferred")); 
							Element predicate_el = (Element)predication.getElementsByTagName("Predicate").item(0);
							srp.predicate = predicate_el.getAttribute("type");
							srp.predicate_text = predicate_el.getAttribute("text");
							String predicate_offset = predicate_el.getAttribute("charOffset");
							if(predicate_offset!=""){
								String[] indices = predicate_offset.split(",");
								for(int in = 0; in<indices.length; in++){
									//TODO expand the object to hold lists here...
									srp.predicate_start_index = Integer.parseInt(indices[in]);
									srp.predicate_end_index = srp.predicate_start_index + srp.predicate_text.length();
								}
							}
							Element subject_el = (Element)predication.getElementsByTagName("Subject").item(0);
							srp.subject_text = subject_el.getAttribute("text");
							srp.subject_pref_name = subject_el.getElementsByTagName("PreferredName").item(0).getTextContent();
							srp.subject_CUI = subject_el.getElementsByTagName("CUI").item(0).getTextContent();
							String sub_offset = subject_el.getAttribute("charOffset");
							if(sub_offset != null){
								String[] indices = sub_offset.split(",");
								for(int in = 0; in<indices.length; in++){
									//TODO and here
									srp.subject_start_index = Integer.parseInt(indices[in]);
									srp.subject_end_index = srp.subject_start_index + srp.subject_text.length();
								}
							}
							NodeList subject_types = subject_el.getElementsByTagName("SemanticTypes").item(0).getChildNodes();
							srp.subject_types = new ArrayList<SemanticType>(subject_types.getLength());
							for(int st = 0; st<subject_types.getLength(); st++){
								if(subject_types.item(st).getNodeType()!=Node.TEXT_NODE){
									Element stype_el = (Element) subject_types.item(st);
									SemanticType stype = new SemanticType();
									stype.name = stype_el.getTextContent();
									srp.subject_types.add(stype);
								}
							}
							Element object_el = (Element)predication.getElementsByTagName("Object").item(0);
							srp.object_text = object_el.getAttribute("text");
							srp.object_pref_name = object_el.getElementsByTagName("PreferredName").item(0).getTextContent();							
							srp.object_CUI = object_el.getElementsByTagName("CUI").item(0).getTextContent();
							String ob_offset = object_el.getAttribute("charOffset");
							if(ob_offset != null){
								String[] indices = ob_offset.split(",");
								for(int in = 0; in<indices.length; in++){
									//TODO and here
									srp.object_start_index = Integer.parseInt(indices[in]);
									srp.object_end_index = srp.object_start_index + srp.object_text.length();
								}
							}
							NodeList object_types = object_el.getElementsByTagName("SemanticTypes").item(0).getChildNodes();
							srp.object_types = new ArrayList<SemanticType>(object_types.getLength());
							for(int st = 0; st<object_types.getLength(); st++){
								if(object_types.item(st).getNodeType()!=Node.TEXT_NODE){
									Element stype_el = (Element) object_types.item(st);
									SemanticType stype = new SemanticType();
									stype.name = stype_el.getTextContent();
									srp.object_types.add(stype);
								}
							}
							if(negated){
								srp.predicate = "NEG_"+srp.predicate;
							}							
							srp.pmid = pdoc.PMID;
							srp.sentence = sentence_text;

							//add definitions from umls
							srp.subject_desc = db.pickOneDef(db.getCUIdefs(srp.subject_CUI));
							srp.object_desc = db.pickOneDef(db.getCUIdefs(srp.object_CUI));

							pdoc.predications.add(srp);
							n_triples++;
						}
					}
				}
				docs.add(pdoc);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}		
		return docs;
	}

	public List<PubmedDoc> getGoldTriples(boolean CUIS){
		List<PubmedDoc> docs = new ArrayList<PubmedDoc>();
		int n_triples = 0;  int ndocs = 0;
		try {

			File fXmlFile = new File("data/semmed_verify/adjudicated_gold.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("MedlineCitation");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node medcite = nList.item(temp);
				Element eElement = (Element) medcite;
				PubmedDoc pdoc = new PubmedDoc();
				pdoc.PMID = Integer.parseInt(eElement.getAttribute("pmid"));
				pdoc.triples = new HashSet<String>();
				NodeList sentences = eElement.getElementsByTagName("Sentence");
				ndocs++;
				for(int s = 0; s < sentences.getLength(); s++){
					Element sentence = (Element)sentences.item(s);
					NodeList predicationss = sentence.getElementsByTagName("Predications");
					for(int p = 0; p < predicationss.getLength(); p++){
						Element predication_node = (Element)predicationss.item(p);
						NodeList predications = predication_node.getElementsByTagName("Predication");
						for(int i=0; i<predications.getLength(); i++){
							Element predication = (Element)predications.item(i);							
							boolean negated = Boolean.parseBoolean(predication.getAttribute("negated"));
							boolean inferred =Boolean.parseBoolean(predication.getAttribute("inferred")); 
							Element predicate_el = (Element)predication.getElementsByTagName("Predicate").item(0);
							String predicate = predicate_el.getAttribute("type");
							Element subject_el = (Element)predication.getElementsByTagName("Subject").item(0);
							String subject_text = subject_el.getAttribute("text");
							String subject_CUI = subject_el.getElementsByTagName("CUI").item(0).getTextContent();

							Element object_el = (Element)predication.getElementsByTagName("Object").item(0);
							String object_text = object_el.getAttribute("text");
							String object_CUI = object_el.getElementsByTagName("CUI").item(0).getTextContent();

							if(negated){
								predicate = "NEG_"+predicate;
							}							

							String combo ="";
							if(CUIS){
								combo = subject_CUI+"_"+predicate+"_"+object_CUI;							
							}else{
								combo = subject_text+"_"+predicate+"_"+object_text;
							}
							pdoc.triples.add(combo);
							n_triples++;
						}

					}
				}
				docs.add(pdoc);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}		
		return docs;
	}
}

