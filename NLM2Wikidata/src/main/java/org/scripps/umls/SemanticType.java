/**
 * 
 */
package org.scripps.umls;

/**
 * @author bgood
 *
 */
public class SemanticType {
	public String name;
	public String abbr;
	public String desc;
}
