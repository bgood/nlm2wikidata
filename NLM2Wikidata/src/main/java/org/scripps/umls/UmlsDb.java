/**
 * 
 */
package org.scripps.umls;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import gov.nih.nlm.uts.webservice.AtomDTO;
import gov.nih.nlm.uts.webservice.Psf;
import gov.nih.nlm.uts.webservice.UtsFault_Exception;

/**
 * Convenience methods to interact with a local installation of the UMLS
 * See http://www.nlm.nih.gov/research/umls/implementation_resources/query_diagrams/
 * Modified the umls distribution by adding the missing table mapping groups to types.  
 * Don't know why they don't include that..
 * 	
	 create table SGROUPS (SGID varchar(50), SGNAME varchar(50), STY_ID varchar(50), STY_NAME varchar(50));
	 load data infile '/Users/bgood/kb2/SemGroups_2013.txt' into table SGROUPS ;
	 
 * @author bgood
 *
 */
public class UmlsDb {

	java.sql.Connection con;
	String dburl = "jdbc:mysql://localhost:3306/umls2016";
	PreparedStatement map2source;
	PreparedStatement typeAbbreviationlookup;
	PreparedStatement getGroupForTypeIds;
	PreparedStatement getGroupForTypeNames;
	PreparedStatement getMeSHCodeFromString;
	PreparedStatement getCuidsFromString;
	PreparedStatement getSemanticTypesFromCUI;
	PreparedStatement getRandomCUIs;
	PreparedStatement getGroupsForCUI;
	PreparedStatement cui2sources;
	PreparedStatement cui2prefTerm;
	PreparedStatement cui2synset;
	public static Set<String> groups;
	
	public UmlsDb() {
		
		groups = new HashSet<String>();
		groups.add("Disorders");
		groups.add("Anatomy");
		groups.add("Physiology");
		groups.add("Chemicals & Drugs");
		groups.add("Living Beings");
		groups.add("Procedures");
		groups.add("Concepts & Ideas");
		groups.add("Devices");
		groups.add("Genes & Molecular Sequences");
		groups.add("Geographic Areas");
		groups.add("Objects");
		groups.add("Occupations");
		groups.add("Phenomena");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch(java.lang.ClassNotFoundException e) {
			System.err.print("ClassNotFoundException: ");
			System.err.println(e.getMessage());
		}
		try {
			con = DriverManager.getConnection(dburl,"root", "");
			map2source = con.prepareStatement("SELECT * FROM MRCONSO WHERE CUI = ? and SAB = ? ");
			
			cui2sources = con.prepareStatement("SELECT CUI, SAB, CODE, STR FROM MRCONSO WHERE CUI = ?");
			
			cui2prefTerm = con.prepareStatement("select * from MRCONSO where CUI = ? and ts = 'P' and stt = 'PF' and ispref = 'Y' and LAT = 'ENG'");
			cui2synset = con.prepareStatement("select distinct(STR) from MRCONSO where CUI = ? and LAT = 'ENG'");
			
			typeAbbreviationlookup = con.prepareStatement("select * from SRDEF where ABR = ?");
			
			getGroupForTypeIds = con.prepareStatement("select * from SGROUPS where STY_ID = ?");
			
			getGroupsForCUI = con.prepareStatement("select SGROUPS.* from SGROUPS, MRSTY where "
					+ "MRSTY.CUI = ? "
					+ "and MRSTY.TUI = SGROUPS.STY_ID ");
			
			getGroupForTypeNames = con.prepareStatement("select * from SGROUPS where STY_NAME = ?");
			getMeSHCodeFromString = con.prepareStatement("select CODE from MRCONSO where SAB = 'MSH' and STR = ?");
			getCuidsFromString = con.prepareStatement("select CUI from MRCONSO where SAB = ? and STR = ?");
			getSemanticTypesFromCUI = con.prepareStatement("select * from MRSTY where CUI = ?");
			getRandomCUIs = con.prepareStatement("select distinct CUI from MRCONSO limit ?");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String cui = "C0012984";//"C0001807";
		String abbr = "SNOMEDCT";//"GO";
		String atom = "Wounds and Injuries";

		UmlsDb d = new UmlsDb();
		
		System.out.println(d.listPrefNameForCUI("C0374299"));
		System.out.println("groups "+d.getGroupForStypeName("Neoplastic Process"));
		System.out.println(d.getSemanticTypesForMeshAtom(atom));
		//		System.out.println(d.getIdsFromSourceForCUI(cui, abbr));
		//		System.out.println(d.getIdsFromSourceForCUI(cui, abbr));
		//		System.out.println(d.getIdsFromSourceForCUI(cui, abbr));
	}

	
	public Set<String> getSynset(String cui){
		Set<String> strs = new HashSet<String>();
		try {
			cui2synset.clearParameters();
			cui2synset.setString(1, cui);
			ResultSet srs = cui2synset.executeQuery();
			while(srs.next()){
				String str = srs.getString("STR"); 
				strs.add(str);
			}
			srs.close();
		}  catch(SQLException ex) {
			System.err.println("-----SQLException-----");
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return strs;
	}
	
	public List<String> getAllCuis(){
		List<String> cuis = new ArrayList<String>();
		try {
			String query = "select distinct cui from MRCONSO";
			ResultSet srs = con.createStatement().executeQuery(query);
			while(srs.next()){
				String cui = srs.getString("cui");
				cuis.add(cui);
			}
			srs.close();
		}  catch(SQLException ex) {
			System.err.println("-----SQLException-----");
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return cuis;
	}	
	
	
	public Set<String> getGroupsFromTypeName(String styname){
		Set<String> groups = new HashSet<String>();
		try {
			String query = "select SGNAME from SGROUPS "
					+ "where STY_NAME ='"+styname+"'";
			ResultSet srs = con.createStatement().executeQuery(query);
			while(srs.next()){
				String group = srs.getString("SGNAME");
				groups.add(group);
			}
			srs.close();
		}  catch(SQLException ex) {
			System.err.println("-----SQLException-----");
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return groups;
	}
	
	public Set<String> getGroupsFromTypeAbreviation(String abr){
		Set<String> groups = new HashSet<String>();
		try {
			String query = "select SGROUPS.* from SGROUPS,SRDEF "
					+ "where SRDEF.UI = SGROUPS.STY_ID and SRDEF.ABR ='"+abr+"'";
			ResultSet srs = con.createStatement().executeQuery(query);
			while(srs.next()){
				String group = srs.getString("SGNAME");
				groups.add(group);
			}
			srs.close();
		}  catch(SQLException ex) {
			System.err.println("-----SQLException-----");
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return groups;
	}
	
	public Set<String> getGroupAbbrsFromTypeAbreviation(String abr){
		Set<String> groups = new HashSet<String>();
		try {
			String query = "select SGROUPS.* from SGROUPS,SRDEF "
					+ "where SRDEF.UI = SGROUPS.STY_ID and SRDEF.ABR ='"+abr+"'";
			ResultSet srs = con.createStatement().executeQuery(query);
			while(srs.next()){
				String group = srs.getString("SGID");
				groups.add(group);
			}
			srs.close();
		}  catch(SQLException ex) {
			System.err.println("-----SQLException-----");
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return groups;
	}
	
	public Set<String> getSemanticTypesFromCUI(String cui){
		Set<String> codes = new HashSet<String>();
		try {
			getSemanticTypesFromCUI.clearParameters();
			getSemanticTypesFromCUI.setString(1, cui);
			ResultSet srs = getSemanticTypesFromCUI.executeQuery();
			while(srs.next()){
				String code = srs.getString("STY"); //actually gives th legible name
				codes.add(code);
			}
			srs.close();
		}  catch(SQLException ex) {
			System.err.println("-----SQLException-----");
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return codes;
	}


	public Set<String> getSemanticTypesForMeshAtom(String term){
	Set<String> roots = new HashSet<String>();
	Set<String> cuis = getCuidsFromMeSHAtom(term);
	if(cuis!=null&&cuis.size()>0){
		for(String cui : cuis){
			Set<String> types = getSemanticTypesFromCUI(cui);
			if(types!=null){
				roots.addAll(types);
			}
		}
	}
	return roots;
	}
	
	/**
	 * See http://www.nlm.nih.gov/mesh/2011/mesh_browser/MeSHtree.html
	 * @param term
	 * @return
	 */
//	public Set<String> getMeshRootsFromAtom(String term){
//		Set<String> roots = new HashSet<String>();
//		Set<String> cuis = getCuidsFromMeSHAtom(term);
//		if(cuis!=null&&cuis.size()>0){
//
//			for(String cui : cuis){
//				Set<String> types = getSourceVocabSemanticTypesFromCUI(cui);
//				if(types!=null){
//					for(String code : types){
//						if(code.toLowerCase().startsWith("a")){
//							roots.add("Anatomy");
//						}else if(code.toLowerCase().startsWith("b")){
//							roots.add("Organisms");
//						}else if(code.toLowerCase().startsWith("c")){
//							roots.add("Diseases");
//						}else if(code.toLowerCase().startsWith("d")){
//							roots.add("Chemicals and Drugs");
//						}else if(code.toLowerCase().startsWith("e")){
//							roots.add("Analytical, Diagnostic and Therapeutic Techniques and Equipment");
//						}else if(code.toLowerCase().startsWith("f")){
//							roots.add("Psychiatry and Psychology");
//						}else if(code.toLowerCase().startsWith("g")){
//							roots.add("Phenomena and Processes");
//						}else if(code.toLowerCase().startsWith("h")){
//							roots.add("Disciplines and Occupations");
//						}else if(code.toLowerCase().startsWith("i")){
//							roots.add("Anthropology, Education, Sociology and Social Phenomena");
//						}else if(code.toLowerCase().startsWith("j")){
//							roots.add("Technology, Industry, Agriculture");
//						}else if(code.toLowerCase().startsWith("k")){
//							roots.add("Humanities");
//						}else if(code.toLowerCase().startsWith("l")){
//							roots.add("Information Science");
//						}else if(code.toLowerCase().startsWith("m")){
//							roots.add("Named Groups");
//						}else if(code.toLowerCase().startsWith("n")){
//							roots.add("Health Care");
//						}else if(code.toLowerCase().startsWith("v")){
//							roots.add("Publication Characteristics");
//						}else if(code.toLowerCase().startsWith("z")){
//							roots.add("Geographicals");
//						}
//					}
//				}
//			}
//		}
//		return roots;
//	}

	public Set<String> getCuidsFromMeSHAtom(String term){
		Set<String> codes = new HashSet<String>();
		try {
			getCuidsFromString.clearParameters();
			getCuidsFromString.setString(1, "MSH");
			getCuidsFromString.setString(2, term);
			ResultSet srs = getCuidsFromString.executeQuery();
			while(srs.next()){
				String code = srs.getString("CUI");
				codes.add(code);
			}
			srs.close();
		}  catch(SQLException ex) {
			System.err.println(" getCuidsFromMeSHAtom -----SQLException----- "+term);
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return codes;
	}

	/**
	 * Return a map of sources to identifiers in that source for the cui
	 * "SELECT CUI, SAB, CODE, STR FROM MRCONSO WHERE CUI = ?"
	 * @param cui
	 * @return
	 * @throws UtsFault_Exception
	 */
	public Map<String, Set<String>> listIdentifiersForCUI(String cui){
		Map<String, Set<String>> source_ids = new HashMap<String, Set<String>>();
		
		try {
			cui2sources.clearParameters();
			cui2sources.setString(1, cui);
			ResultSet srs = cui2sources.executeQuery();
			while(srs.next()){
				String source = srs.getString("SAB");
				Set<String> ids = source_ids.get(source);
				if(ids==null){
					ids = new HashSet<String>();
				}
				String id = srs.getString("CODE");
				ids.add(id); 
				source_ids.put(source, ids);
			}
			srs.close();
		}  catch(SQLException ex) {
			System.err.println(" getCuidsFromMeSHAtom -----SQLException----- "+cui);
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return source_ids;
	}
	
	/**
	 * note that 
	 * @param cui
	 * @return
	 * @throws UtsFault_Exception
	 */
	public String listPrefNameForCUI(String cui) {
		String name = "";
		
		try {
			cui2prefTerm.clearParameters();
			cui2prefTerm.setString(1, cui);
			ResultSet srs = cui2prefTerm.executeQuery();
			if(srs.next()){
				name = srs.getString("STR");
			}
			srs.close();
		}  catch(SQLException ex) {
			System.err.println(" getCuidsFromMeSHAtom -----SQLException----- "+cui);
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return name;
	}
	
	
	public Set<String> getMeshCodesFromAtom(String term){
		Set<String> codes = new HashSet<String>();
		try {
			getMeSHCodeFromString.clearParameters();
			getMeSHCodeFromString.setString(1, term);
			ResultSet srs = getMeSHCodeFromString.executeQuery();
			while(srs.next()){
				String code = srs.getString("CODE");
				codes.add(code);
			}
			srs.close();
		}  catch(SQLException ex) {
			System.err.println(" getMeshCodesFromAtom -----SQLException----- "+term);
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return codes;
	}

	public List<String> getIdsFromSourceForCUI(String CUI, String source_vocab_abbr){
		List<String> ids = new ArrayList<String>();

		try {
			map2source.clearParameters();
			map2source.setString(1, CUI);
			map2source.setString(2, source_vocab_abbr);
			ResultSet srs = map2source.executeQuery();
			while(srs.next()){
				ids.add(srs.getString("CODE"));
			}
			srs.close();
		}  catch(SQLException ex) {
			System.err.println("getIdsFromSourceForCUI-----SQLException----- "+CUI);
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return ids;
	}
	
	public String getSemanticTypeInfoFromAbbreviation(String abbr){
		String type = "";

		try {
			typeAbbreviationlookup.clearParameters();
			typeAbbreviationlookup.setString(1, abbr);
			ResultSet srs = typeAbbreviationlookup.executeQuery();
			if(srs.next()){
				type = srs.getString("STY_RL")+"\t"+srs.getString("UI");
			}else{
				type = null;
			}

			srs.close();
		}  catch(SQLException ex) {
			System.err.println("getSemanticTypeInfoFromAbbreviation -----SQLException----- "+abbr);
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return type;
	}

	public String getGroupForStype(String type_id){
		String type = "";

		try {
			getGroupForTypeIds.clearParameters();
			getGroupForTypeIds.setString(1, type_id);
			ResultSet srs = getGroupForTypeIds.executeQuery();
			if(srs.next()){
				type = srs.getString("SGNAME");
			}else{
				type = null;
			}

			srs.close();
		}  catch(SQLException ex) {
			System.err.println("getGroupForStype -----SQLException----- "+type_id);
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return type;
	}
	public String getGroupForStypeName(String type_name){
		String type = "";

		try {
			getGroupForTypeNames.clearParameters();
			getGroupForTypeNames.setString(1, type_name);
			ResultSet srs = getGroupForTypeNames.executeQuery();
			if(srs.next()){
				type = srs.getString("SGNAME");
			}else{
				type = null;
			}

			srs.close();
		}  catch(SQLException ex) {
			System.err.println("getGroupForStypeName -----SQLException----- "+type_name);
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return type;
	}	
	public Set<String> getGroupsForCUI(String cui, boolean abbr){
		Set<String> groups = new HashSet<String>();

		try {
			getGroupsForCUI.clearParameters();
			getGroupsForCUI.setString(1, cui);
			ResultSet srs = getGroupsForCUI.executeQuery();
			while(srs.next()){
				if(abbr){
					groups.add(srs.getString("SGID"));
				}else{
					groups.add(srs.getString("SGNAME"));
				}
			}

			srs.close();
		}  catch(SQLException ex) {
			System.err.println("getGroupForStypeName -----SQLException----- ");
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return groups;
	}	

	public Set<String> getRandomCuis(int limit){
		Set<String> cuis = new HashSet<String>();

		try {
			getRandomCUIs.clearParameters();
			getRandomCUIs.setInt(1, limit);
			ResultSet cuiss = getRandomCUIs.executeQuery();
			while(cuiss.next()){
				cuis.add(cuiss.getString("CUI"));
			}
			cuiss.close();
		}  catch(SQLException ex) {
			System.err.println("SQLState:  " + ex.getSQLState());
			System.err.println("Message:  " + ex.getMessage());
			System.err.println("Vendor:  " + ex.getErrorCode());
		}
		return cuis;
	}	
	

	public java.sql.Connection getCon() {
		return con;
	}


	public void setCon(java.sql.Connection con) {
		this.con = con;
	}

}
