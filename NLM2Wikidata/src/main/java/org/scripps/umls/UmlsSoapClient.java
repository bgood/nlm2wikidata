/**
 * 
 */
package org.scripps.umls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.scripps.kb.WikidataSparqlClient;

import gov.nih.nlm.uts.webservice.AtomDTO;
import gov.nih.nlm.uts.webservice.ConceptDTO;
import gov.nih.nlm.uts.webservice.Psf;
import gov.nih.nlm.uts.webservice.SourceDTO;
import gov.nih.nlm.uts.webservice.UiLabel;
import gov.nih.nlm.uts.webservice.UiLabelRootSource;
import gov.nih.nlm.uts.webservice.UtsFault_Exception;
import gov.nih.nlm.uts.webservice.UtsWsContentController;
import gov.nih.nlm.uts.webservice.UtsWsContentControllerImplService;
import gov.nih.nlm.uts.webservice.UtsWsFinderController;
import gov.nih.nlm.uts.webservice.UtsWsFinderControllerImplService;
import gov.nih.nlm.uts.webservice.UtsWsHistoryController;
import gov.nih.nlm.uts.webservice.UtsWsHistoryControllerImplService;
import gov.nih.nlm.uts.webservice.UtsWsMetadataController;
import gov.nih.nlm.uts.webservice.UtsWsMetadataControllerImplService;
import gov.nih.nlm.uts.webservice.UtsWsSecurityController;
import gov.nih.nlm.uts.webservice.UtsWsSecurityControllerImplService;
import gov.nih.nlm.uts.webservice.UtsWsSemanticNetworkController;
import gov.nih.nlm.uts.webservice.UtsWsSemanticNetworkControllerImplService;

/**
 * @author bgood
 *
 */
public class UmlsSoapClient {
	private UtsWsSecurityController utsSecurityService;
	private UtsWsContentController utsContentService;
	private UtsWsFinderController utsFinderService;
	private UtsWsMetadataController utsMetadataService;
	private UtsWsSemanticNetworkController utsSemanticNetworkService;
	private UtsWsHistoryController utsHistoryService;
	private static String user = "user";
	private static String pw = "password";
	private String ticketGrantingTicket;
	private String serviceName = "http://umlsks.nlm.nih.gov";
	private String currentUmlsRelease;

	/**
	 * @param args
	 */

	public UmlsSoapClient(){
		init();
	}

	public void init(){

		try {
			utsSecurityService = (new UtsWsSecurityControllerImplService()).getUtsWsSecurityControllerImplPort();
			ticketGrantingTicket = utsSecurityService.getProxyGrantTicket(user, pw);

			utsContentService = (new UtsWsContentControllerImplService()).getUtsWsContentControllerImplPort();
			utsFinderService = (new UtsWsFinderControllerImplService()).getUtsWsFinderControllerImplPort();
			utsMetadataService = (new UtsWsMetadataControllerImplService()).getUtsWsMetadataControllerImplPort();
			currentUmlsRelease = utsMetadataService.getCurrentUMLSVersion(getProxyTicket());

			utsSemanticNetworkService = (new UtsWsSemanticNetworkControllerImplService()).getUtsWsSemanticNetworkControllerImplPort();
			utsHistoryService = (new UtsWsHistoryControllerImplService()).getUtsWsHistoryControllerImplPort();		
		}

		catch (Exception e) {
			System.out.println("Error!!!" + e.getMessage());
		}
	}

	private String getProxyTicket() {
		try {
			return utsSecurityService.getProxyTicket(ticketGrantingTicket, serviceName);
		}
		catch (Exception e) {
			return "";
		}
	}


	public static void main(String[] args) throws UtsFault_Exception {

		UmlsSoapClient c = new UmlsSoapClient();
		//		String id = "D014147";// "D014147" //"9468002"; //
		//		String source = "MSH";
		//		Set<ConceptDTO> concepts = c.getCuiConceptsBySourceId(source,id);
		//		for(ConceptDTO concept : concepts){
		//			System.out.println(c.getCUIString(concept));
		//		}

		//nuria		
		//Zydol [A26639650/MSH/PEP/D014147]
		//Ultram [A26628821/MSH/PEP/D014147]
		//K 315 [A0426578/MSH/PM/D014147]		
		//K-315 [A26634376/MSH/PEP/D014147]
		//K315 [A1925859/MSH/PM/D014147]



		//ben		
		//Tramadol [A0127407/MSH/MH/D014147]		
		//Byk Brand of Tramadol Hydrochloride [A26677207/MSH/ET/D014147]		
		//1A Brand of Tramadol Hydrochloride [A26631604/MSH/PEP/D014147]
		//Ranitidin 1A Pharma [A26663715/MSH/ET/D014147]
		//Trama 1A Pharma [A26639651/MSH/ET/D014147]
		//Tramadol 1A [A26604430/MSH/ET/D014147]		



		String cui = "C0592292";// "C0040610"; 
		Map<String, Set<String>> source_ids = c.listAllIdentifiersForCUI(cui);

		for(String source :source_ids.keySet()){
			Set<String> ids = source_ids.get(source);
			for(String id : ids){
				Set<String> wikidata_items = WikidataSparqlClient.getItemsByIdProp("P486", id);
				System.out.println(id+"\t"+wikidata_items);
			}
		}

	}


	void listCurrentUmlsSources() throws UtsFault_Exception{
		List<SourceDTO> sources = utsMetadataService.getAllSources(getProxyTicket(), currentUmlsRelease);
		for (SourceDTO source:sources) {				
			System.out.println(
					source.getRootSource().getPreferredName()+"\t"+
							source.getRootSource().getAbbreviation()+"\t"+
							source.getRootSource().getFamily()+"\t"+
							source.getRootSource().getRestrictionLevel()+"\t"+
							source.getRootSource().getLanguage().getExpandedForm()+"\t"+
							source.getConceptCount()+"\t"+source.getAtomCount()+"\t"+source.getVersion());
		}
	}

	Set<ConceptDTO> getCuiConceptsBySourceId(String source, String id) throws UtsFault_Exception{
		Set<String> cuis = new HashSet<String>();
		Set<ConceptDTO> concepts = new HashSet<ConceptDTO>();
		Psf myPsf = new Psf();
		List<AtomDTO> myAtoms = new ArrayList<AtomDTO>();
		myAtoms = utsContentService.getSourceDescriptorAtoms(
				getProxyTicket(), currentUmlsRelease,id,source,myPsf);
		for (AtomDTO atom:myAtoms) {
			if(cuis.add(atom.getConcept().getUi())){
				concepts.add(atom.getConcept());
			}
		}
		return concepts;
	}

	ConceptDTO getConceptByCUI(String cui) throws UtsFault_Exception{
		ConceptDTO concept = utsContentService.getConcept(getProxyTicket(), currentUmlsRelease, cui);
		return concept;
	}

	Map<String, Set<String>> listAllIdentifiersForCUI(String cui) throws UtsFault_Exception{
		Map<String, Set<String>> source_ids = new HashMap<String, Set<String>>();
		Psf myPsf = new Psf();
		//exclude obsolete and suppressible atoms
		myPsf.setIncludeSuppressible(false);
		myPsf.setIncludeObsolete(false);

		//only include SNOMEDCT_US and OMIM source vocabularies
		//	myPsf.getIncludedSources().add("SNOMEDCT_US");
		//	myPsf.getIncludedSources().add("OMIM");
		//  myPsf.getIncludedSources().add("MSH");

		List<AtomDTO> atoms = new ArrayList<AtomDTO>();
		atoms = utsContentService.getConceptAtoms(getProxyTicket(), currentUmlsRelease, cui, myPsf);

		for (AtomDTO atom:atoms) {
			//System.out.println(getAtomString(atom));
			//ids.add(atom.getCode().getUi());
			String source = atom.getRootSource();
			Set<String> ids = source_ids.get(source);
			if(ids==null){
				ids = new HashSet<String>();
			}
			ids.add(atom.getCode().getUi()); //TODO check this to make sure it gets everything
			source_ids.put(source, ids);
		}
		return source_ids;
	}

	public String getAtomString(AtomDTO atom){
		String aui = atom.getUi();
		String tty = atom.getTermType();
		String name = atom.getTermString().getName();
		String sourceId = atom.getCode().getUi();
		String rsab = atom.getRootSource();

		return(aui+"\t"+tty+"\t"+name+"\t"+sourceId+"\t"+rsab);
	}

	public String getCUIString(ConceptDTO concept){
		String name = concept.getDefaultPreferredName();
		List<String> semanticTypes = concept.getSemanticTypes();
		int numberofAtoms = concept.getAtomCount();
		String out = concept.getUi()+"\t"+name+" "+semanticTypes+" "+numberofAtoms;
		return out;
	}

}
