# README #

This code hacks together mappings from the UMLS to Wikidata.  In particular it was created to allow for the export of SemmedDB predications in the form of Wikidata claims.  Look at SemmedExtractor.java to get your bearings.  

Requires a local mysql version of the [UMLS](https://www.nlm.nih.gov/research/umls/) and [SemmedDB](https://skr3.nlm.nih.gov/SemMedDB/) last tested on 2016 versions.  (With minor extensions (new tables) noted in their access classes SemDb and UmlsDB .